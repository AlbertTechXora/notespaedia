import 'dart:io';

class SharedFunctions {
  static connectionCheck() async {
    var connectionStatus;
    try {
      final result = await InternetAddress.lookup('google.com');
      print("result: $result");
      if (result.isEmpty && result[0].rawAddress.isEmpty) {
        connectionStatus = "No Internet Connection.";
      } else {
        connectionStatus = null;
      }
    } on SocketException catch (_) {
      connectionStatus = "You are not Connected to the Internet.";
    }
    return connectionStatus;
  }
}
