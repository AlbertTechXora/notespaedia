import 'package:sqflite/sqflite.dart';

abstract class LocalDB {
  static Database _db;
  static Database get db => _db;

  static Future<void> init() async {
    var databasesPath = await getDatabasesPath();
    String path = databasesPath + 'documents.db';
    _db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the following tables
      await db.execute(
        'CREATE TABLE Documents ('
        'id INTEGER PRIMARY KEY, '
        'name TEXT, '
        'current_page INTEGER'
        ')',
      );
      await db.execute(
        'CREATE TABLE Notes ('
        'id INTEGER PRIMARY KEY, '
        'type TEXT, '
        'document_name TEXT, '
        'page INTEGER, '
        'points TEXT, '
        'text TEXT, '
        'imgName TEXT, '
        'imgPath TEXT, '
        'x REAL, '
        'y REAL, '
        'color INTEGER'
        ')',
      );
      await db.execute(
        'CREATE TABLE LibraryCache ('
        'id INTEGER PRIMARY KEY, '
        'path TEXT, '
        'thumbnail BLOB'
        ')',
      );
    });
  }
}
