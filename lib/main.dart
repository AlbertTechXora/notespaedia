import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/services.dart';
import 'package:flutter_color/flutter_color.dart';

import './views/onBoardingScreen.dart';
import './logic/local_db/local_db.dart';

void main() async {
  // Required by the Flutter framework
  WidgetsFlutterBinding.ensureInitialized();

  // Initialise local SQLite database
  await LocalDB.init();

  final savedThemeMode = await AdaptiveTheme.getThemeMode();
  runApp(MyApp(savedThemeMode: savedThemeMode));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final AdaptiveThemeMode savedThemeMode;

  const MyApp({Key key, this.savedThemeMode}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.transparent,
    //   // systemNavigationBarColor: Colors.transparent,
    // ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return AdaptiveTheme(
          light: ThemeData(
            primaryColor: HexColor("#4AC8C7"),
            primaryColorLight: HexColor("#C5ECEB"),
            fontFamily: "Stolzl",
            scaffoldBackgroundColor: HexColor("#FEF5F3"),
            textTheme: TextTheme(
              headline1: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 24.0 : 20.0,
                height: 1.4,
                color: HexColor("#002231"),
              ),
              headline2: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 28.0 : 24.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline3: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 24.0 : 20.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline4: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 22.0 : 18.0,
                height: 1.4,
                color: HexColor("#002231"),
              ),
              headline5: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontSize: maxWidth > 600 ? 18.0 : 14.0,
                height: 1.4,
                color: HexColor("#4AC8C7"),
              ),
              headline6: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontSize: maxWidth > 600 ? 18.0 : 14.0,
                height: 1.4,
                color: HexColor("#4B4C4D"),
              ),
              bodyText1: TextStyle(
                color: HexColor("#4B4C4D"),
                fontSize: maxWidth > 600 ? 16.0 : 14.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              bodyText2: TextStyle(
                color: HexColor("#8E8E8E"),
                fontSize: maxWidth > 600 ? 16.0 : 14.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              subtitle1: TextStyle(
                color: HexColor("#B7B7B7"),
                fontSize: maxWidth > 600 ? 14.0 : 12.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              subtitle2: TextStyle(
                color: HexColor("#002231"),
                fontSize: maxWidth > 600 ? 14.0 : 12.0,
                height: 1.4,
                fontWeight: FontWeight.w500,
              ),
            ),
            buttonTheme: ButtonThemeData(
              buttonColor: HexColor("#4AC8C7"),
              minWidth: 160,
              height: 38,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                  HexColor("#4AC8C7"),
                ),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
                minimumSize: MaterialStateProperty.all<Size>(Size(160.0, 38.0)),
              ),
            ),
            appBarTheme: AppBarTheme(
              elevation: 0,
              actionsIconTheme: IconThemeData(
                color: Colors.white,
              ),
            ),
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              selectedItemColor: HexColor("#4AC8C7"),
              unselectedItemColor: Colors.grey,
              showUnselectedLabels: false,
            ),
          ),
          dark: ThemeData(
            primaryColor: Colors.black,
            primaryColorLight: Colors.grey[800],
            fontFamily: "Stolzl",
            scaffoldBackgroundColor: Colors.black,
            cardColor: Colors.grey,
            bottomAppBarColor: Colors.black,
            textTheme: TextTheme(
              headline1: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 24.0 : 20.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline2: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 28.0 : 24.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline3: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 24.0 : 20.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline4: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontWeight: FontWeight.bold,
                fontSize: maxWidth > 600 ? 22.0 : 18.0,
                height: 1.4,
                color: Colors.white,
              ),
              headline5: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontSize: maxWidth > 600 ? 18.0 : 14.0,
                height: 1.4,
                color: HexColor("#4AC8C7"),
              ),
              headline6: TextStyle(
                fontFamily: "assets/fonts/Stolzl-Bold.ttf",
                fontSize: maxWidth > 600 ? 18.0 : 14.0,
                height: 1.4,
                color: HexColor("#4B4C4D"),
              ),
              bodyText1: TextStyle(
                color: HexColor("#4B4C4D"),
                fontSize: maxWidth > 600 ? 16.0 : 14.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              bodyText2: TextStyle(
                color: HexColor("#8E8E8E"),
                fontSize: maxWidth > 600 ? 16.0 : 14.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              subtitle1: TextStyle(
                color: HexColor("#B7B7B7"),
                fontSize: maxWidth > 600 ? 14.0 : 12.0,
                height: 1.4,
                fontWeight: FontWeight.normal,
              ),
              subtitle2: TextStyle(
                color: Colors.black,
                fontSize: maxWidth > 600 ? 14.0 : 12.0,
                height: 1.4,
                fontWeight: FontWeight.w500,
              ),
            ),
            buttonTheme: ButtonThemeData(
              buttonColor: Colors.black,
              minWidth: 160,
              height: 38,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                  Colors.black,
                ),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
                minimumSize: MaterialStateProperty.all<Size>(Size(160.0, 38.0)),
              ),
            ),
            appBarTheme: AppBarTheme(
              elevation: 0,
              actionsIconTheme: IconThemeData(
                color: Colors.white,
              ),
            ),
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey,
              showUnselectedLabels: false,
            ),
          ),
          initial: savedThemeMode ?? AdaptiveThemeMode.light,
          builder: (theme, darkTheme) => MaterialApp(
            title: 'Notespaedia',
            debugShowCheckedModeBanner: false,
            theme: theme,
            darkTheme: darkTheme,
            home: OnBoardingSreen(),
          ),
        );
      },
    );
  }
}
