import 'dart:typed_data' show Uint8List;

class FileModel {
  final String path;
  final Uint8List thumbnail;

  FileModel({this.path, this.thumbnail});
}
