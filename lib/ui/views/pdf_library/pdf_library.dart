import 'package:flutter/material.dart';

import '../../../logic/local_db/local_db.dart';
import '../../../ui/views/pdf_library/file_model.dart';
import '../../../ui/views/pdf_reader/pdf_reader.dart';

class PDFLibrary extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PDFLibraryState();
  }
}

class _PDFLibraryState extends State<PDFLibrary> {
  Future<List> _getLibrary() async {
    List results = await LocalDB.db.rawQuery('SELECT * FROM LibraryCache');

    List<FileModel> available = [
      for (var result in results)
        FileModel(
          path: result['path'],
          thumbnail: result['thumbnail'],
        ),
    ];

    // TODO: Remove everything except return in production
    bool _found = false;

    for (var file in available) {
      if (file.path == 'assets/sample.pdf') {
        _found = true;
        break;
      }
    }

    if (!_found) {
      await LocalDB.db.insert(
        'LibraryCache',
        {'path': 'assets/sample.pdf'},
      );
      available.add(FileModel(path: 'assets/sample.pdf'));
    }

    return available;
  }

  Key _key = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black87,
        brightness: Brightness.dark,
        title: Text(
          'My files',
          style: const TextStyle(color: Colors.white),
        ),
      ),
      body: FutureBuilder(
        key: _key,
        future: _getLibrary(),
        builder: (context, content) => content.hasError
            ? Center(child: Text(content.error.toString()))
            : content.connectionState == ConnectionState.done
                ? content.data.isNotEmpty
                    ? ListView(
                        children: [
                          for (var document in content.data)
                          
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
                              child: GestureDetector(
                                child: DecoratedBox(
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    boxShadow: kElevationToShadow[2],
                                    image: document.thumbnail != null
                                        ? DecorationImage(
                                            image: MemoryImage(
                                              document.thumbnail,
                                            ),
                                          )
                                        : null,
                                  ),
                                  child: SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height:
                                        MediaQuery.of(context).size.height / 3,
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: DecoratedBox(
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: Padding(
                                          padding: const EdgeInsets.all(10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(document.path),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context)
                                      .push(
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              PDFReader(path: document.path),
                                        ),
                                      )
                                      .whenComplete(() =>
                                          setState(() => _key = UniqueKey()));
                                },
                              ),
                            ),
                        ],
                      )
                    : Center(child: Text('No documents found'))
                : Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
