import 'dart:async';

abstract class Notes {
  static StreamController<bool> _streamController;

  static void init() => _streamController = StreamController.broadcast();

  static Stream get stream => _streamController.stream;

  static void newNote() => _streamController.add(true);

  static void dispose() => _streamController.close();
}
