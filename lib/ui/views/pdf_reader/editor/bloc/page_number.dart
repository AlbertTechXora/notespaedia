import 'dart:async';

abstract class PageNumber {
  static int _number = 1;
  static int get number => _number;

  static StreamController<int> _streamController;

  static void init() => _streamController = StreamController.broadcast();

  static Stream<int> get stream => _streamController.stream;

  static void change(int number) {
    _streamController.add(number);
    _number = number;
  }

  static void dispose() {
    _streamController.close();
    _number = 1;
  }
}
