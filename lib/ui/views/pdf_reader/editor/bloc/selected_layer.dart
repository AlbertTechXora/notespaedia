import 'dart:async';

abstract class SelectedLayer {
  static StreamController _streamController;

  static void init() => _streamController = StreamController.broadcast();

  static Stream get stream => _streamController.stream;

  static void change(layer) => _streamController.add(layer);

  static void dispose() => _streamController.close();
}
