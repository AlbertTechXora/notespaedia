import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../bloc/notes.dart';
import '../bloc/page_number.dart';
import '../bloc/selected_layer.dart';
import '../scribble/scribble.dart';
import '../text/add_text.dart';
import '../../../../../logic/local_db/local_db.dart';

class AddButton extends StatefulWidget {
  final Function cancelTimer;
  final Function(bool) showEditor;
  final String documentName;

  AddButton({
    @required this.cancelTimer,
    @required this.showEditor,
    @required this.documentName,
  });

  @override
  State<StatefulWidget> createState() {
    return _AddButtonState();
  }
}

class _AddButtonState extends State<AddButton>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _rotation;
  Animation<double> _offset;
  Animation<double> _opacity;

  List imgPickerOptions = [
    {"iconData": Icons.camera, "optionsLabel": "Camera"},
    {"iconData": Icons.photo, "optionsLabel": "Gallery"}
  ];

  //Image Picking
  File imageFile;
  PickedFile _img;
  final ImagePicker _picker = ImagePicker();
  getProfileImg(src) async {
    final image = await _picker.getImage(source: src);
    setState(() {
      _img = image;
    });
    imageFile = File(_img.path);
    print("_img:=> $_img");
    print("imageFile:=> $imageFile");
    // get file length
    var length = await imageFile.length();

    // Insert note into local storage
    await LocalDB.db.insert(
      'Notes',
      {
        'document_name': widget.documentName,
        'page': PageNumber.number,
        'type': 'image',
        'imgName': imageFile,
        'imgPath': imageFile,
        'x': '',
        'y': '',
      },
    );
  }
  //Image picking End

  showImgPickerOptionsBottomSheet() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),
      context: context,
      builder: (BuildContext _) {
        return Container(
          // height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              primary: true,
              itemCount: imgPickerOptions.length,
              // itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: Icon(imgPickerOptions[index]['iconData']),
                  title: Text(
                    imgPickerOptions[index]['optionsLabel'],
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  onTap: () {
                    if (imgPickerOptions[index]['optionsLabel'] == "Camera") {
                      getProfileImg(ImageSource.camera);
                    } else {
                      getProfileImg(ImageSource.gallery);
                    }
                    Navigator.pop(context);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            ),
          ),
        );
      },
      isScrollControlled: true,
    );
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    )..addListener(() => setState(() {}));
    _rotation = Tween(begin: 0.0, end: 45.0).animate(_animationController);
    _offset = Tween(begin: 200.0, end: 0.0).animate(_animationController);
    _opacity = Tween(begin: 0.0, end: 1.0).animate(_animationController);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (_opacity.value != 0)
          Opacity(
            opacity: _opacity.value,
            child: Transform.translate(
              offset: Offset(0, _offset.value),
              child: IconButton(
                icon: Icon(Icons.edit),
                onPressed: () async {
                  widget.showEditor(false);
                  SelectedLayer.change(null);
                  await showDialog(
                    context: context,
                    barrierDismissible: false,
                    barrierColor: Colors.transparent,
                    builder: (context) => Scribble(
                      documentName: widget.documentName,
                      page: PageNumber.number,
                    ),
                  ).then(
                    (value) {
                      if (value != null) Notes.newNote();
                    },
                  );
                  widget.showEditor(true);
                  // Set automatic timer to hide the options
                },
              ),
            ),
          ),
        if (_opacity.value != 0)
          Opacity(
            opacity: _opacity.value,
            child: Transform.translate(
              offset: Offset(0, _offset.value),
              child: IconButton(
                icon: Icon(Icons.text_fields),
                onPressed: () async {
                  widget.showEditor(false);
                  SelectedLayer.change(null);
                  await showDialog(
                    context: context,
                    barrierDismissible: false,
                    barrierColor: Colors.transparent,
                    builder: (context) => AddText(
                      documentName: widget.documentName,
                      page: PageNumber.number,
                      initialOffset: Offset(
                        MediaQuery.of(context).size.width * 0.165,
                        MediaQuery.of(context).size.height / 4,
                      ),
                    ),
                  ).then(
                    (value) {
                      if (value != null) Notes.newNote();
                    },
                  );
                  widget.showEditor(true);
                  // Set automatic timer to hide the options
                },
              ),
            ),
          ),
        if (_opacity.value != 0)
          Opacity(
            opacity: _opacity.value,
            child: Transform.translate(
              offset: Offset(0, _offset.value),
              child: IconButton(
                icon: Icon(Icons.add_photo_alternate_outlined),
                // onPressed: showImgPickerOptionsBottomSheet,
                onPressed: () {
                  getProfileImg(ImageSource.gallery);
                },
              ),
            ),
          ),
        RotationTransition(
          turns: AlwaysStoppedAnimation(_rotation.value / 360),
          child: IconButton(
            icon: Icon(Icons.add, size: 34),
            onPressed: () {
              if (_animationController.status == AnimationStatus.completed) {
                _animationController.reverse();
              } else {
                _animationController.forward();
                widget.cancelTimer();
              }
            },
          ),
        ),
      ],
    );
  }
}
