import 'dart:async';
import 'package:flutter/material.dart';

import '../../editor/bloc/page_number.dart';
import '../../editor/display/add_button.dart';

class EditorOptions extends StatefulWidget {
  final Function(bool) showEditor;
  final String documentName;
  final int pageCount;

  EditorOptions({
    @required this.showEditor,
    @required this.documentName,
    @required this.pageCount,
  });

  @override
  State<StatefulWidget> createState() {
    return _EditorOptionsState();
  }
}

class _EditorOptionsState extends State<EditorOptions>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _opacity;

  Timer _timer;

  void _setTimer(AnimationController controller) => _timer = Timer(
        const Duration(seconds: 2),
        () => controller.reverse().whenComplete(() => widget.showEditor(false)),
      );

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    )..addListener(() => setState(() {}));
    _opacity = Tween<double>(begin: 0, end: 1).animate(_animationController);
    _animationController.forward();
    _setTimer(_animationController);
  }

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: _opacity.value,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: DecoratedBox(
                  decoration: BoxDecoration(),
                  child: StreamBuilder(
                    stream: PageNumber.stream,
                    initialData: 1,
                    builder: (context, page) => RichText(
                      text: TextSpan(
                        style:
                            const TextStyle(fontSize: 18, color: Colors.black),
                        children: [
                          TextSpan(
                            text: page.data.toString(),
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: ' I ',
                            style: const TextStyle(
                              fontWeight: FontWeight.w100,
                              color: Colors.grey,
                            ),
                          ),
                          TextSpan(
                            text: widget.pageCount.toString(),
                            style: const TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.list),
                    onPressed: () => null,
                  ),
                  IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () => null,
                  ),
                ],
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 16, bottom: 16),
              child: AddButton(
                cancelTimer: _timer.cancel,
                showEditor: widget.showEditor,
                documentName: widget.documentName,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
