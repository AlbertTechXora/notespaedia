import 'package:flutter/material.dart';

import '../../../../logic/local_db/local_db.dart';
import '../editor/bloc/notes.dart';
import '../editor/bloc/selected_layer.dart';

class LayerOptionButton extends StatelessWidget {
  final IconData icon;
  final Function onTap;

  LayerOptionButton({@required this.icon, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: DecoratedBox(
        decoration: BoxDecoration(
          boxShadow: kElevationToShadow[1],
          shape: BoxShape.circle,
          color: onTap == null ? Colors.grey.shade300 : Colors.white,
        ),
        child: SizedBox(
          height: 44,
          width: 44,
          child: Center(child: Icon(icon)),
        ),
      ),
      onTap: onTap,
    );
  }
}

class LayerOptions extends StatelessWidget {
  final int id;
  final String type;

  LayerOptions({@required this.id, @required this.type});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LayerOptionButton(
                icon: Icons.edit,
                onTap: type == 'scribble' ? null : () async {},
              ),
              const SizedBox(width: 16),
              LayerOptionButton(
                icon: Icons.delete,
                onTap: () async {
                  await LocalDB.db
                      .rawDelete('DELETE FROM Notes WHERE id = ?', [id]);
                  SelectedLayer.change(null);
                  Notes.newNote();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
