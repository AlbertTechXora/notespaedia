import 'dart:async';

abstract class ColorSelection {
  static StreamController _streamController;

  static void init() => _streamController = StreamController.broadcast();

  static Stream get stream => _streamController.stream;

  static void change(color) => _streamController.add(color);

  static void dispose() => _streamController.close();
}
