import 'package:flutter/material.dart';
import 'bloc/color_selection.dart';

class ColorButton extends StatelessWidget {
  final int value;

  ColorButton({@required this.value});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: StreamBuilder(
          stream: ColorSelection.stream,
          initialData: 0XFFFF5733,
          builder: (context, selected) => Stack(
            alignment: Alignment.center,
            children: [
              DecoratedBox(
                decoration: BoxDecoration(
                  color: Color(value),
                  shape: BoxShape.circle,
                ),
                child: SizedBox(width: 26, height: 26),
              ),
              if (selected.hasData && selected.data == value)
                DecoratedBox(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white70,
                  ),
                  child: SizedBox(width: 8, height: 8),
                ),
            ],
          ),
        ),
      ),
      onTap: () => ColorSelection.change(value),
    );
  }
}
