import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';

import '../../../../../logic/local_db/local_db.dart';
import '../../editor/scribble/colors/bloc/color_selection.dart';
import '../scribble/colors/color_button.dart';
import '../scribble/scribble_painter.dart';

class Scribble extends StatefulWidget {
  final String documentName;
  final int page;

  Scribble({@required this.documentName, @required this.page});

  ScribbleState createState() => ScribbleState();
}

class ScribbleState extends State<Scribble> {
  final StreamController _linesController = StreamController.broadcast();

  List<Offset> _points = <Offset>[];

  Color _paintColor = Color(0xffff5733);

  StreamSubscription _colorSubscription;

  @override
  void initState() {
    super.initState();
    ColorSelection.init();
    _colorSubscription = ColorSelection.stream.listen(
      (value) {
        _paintColor = Color(value);
        _linesController.add(_points);
      },
    );
  }

  List<List<Offset>> _latestBackup = [];

  bool _savingNote = false;

  Future<void> _saveNote() async {
    _savingNote = true;

    String pointsJson = jsonEncode(
      [
        for (var offset in _points)
          offset == null ? null : {'x': offset.dx, 'y': offset.dy}
      ],
    );

    // Insert note into local storage
    await LocalDB.db.insert(
      'Notes',
      {
        'document_name': widget.documentName,
        'page': widget.page,
        'points': pointsJson,
        'type': 'scribble',
        'color': _paintColor.value,
      },
    );
  }

  void _undo() {
    _latestBackup.last
        .forEach((latest) => _points.removeWhere((e) => e == latest));
    _latestBackup.removeLast();
    _linesController.add(_points);
  }

  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: [
          StreamBuilder(
            stream: _linesController.stream,
            initialData: _points,
            builder: (context, points) => CustomPaint(
              painter: ScribblePainter(points.data, _paintColor.value),
            ),
          ),
          GestureDetector(
            onPanStart: (details) {
              _latestBackup.add(<Offset>[]);
              final RenderBox referenceBox = context.findRenderObject();
              final Offset localPosition =
                  referenceBox.globalToLocal(details.globalPosition);
              _points = List.from(_points)..add(localPosition);
              _latestBackup.last.add(localPosition);
              _linesController.add(_points);
            },
            onPanUpdate: (details) {
              final RenderBox referenceBox = context.findRenderObject();
              final Offset localPosition =
                  referenceBox.globalToLocal(details.globalPosition);
              double offsetDifference =
                  _points.last.distanceSquared - localPosition.distanceSquared;
              if (offsetDifference < 0)
                offsetDifference = offsetDifference * -1;
              // Below clause prevents multitouch drawing
              if (_points.isNotEmpty &&
                  _points.last != null &&
                  offsetDifference < 20000) {
                _points = List.from(_points)..add(localPosition);
                _linesController.add(_points);
              }
              _latestBackup.last.add(localPosition);
            },
            onPanEnd: (_) {
              if (_latestBackup.last.length < 10)
                _undo();
              else
                _points.add(null);
            },
          ),
          Positioned(
            right: 0,
            child: IconButton(
              icon: Icon(Icons.close),
              onPressed: () => Navigator.pop(context),
            ),
          ),
          Positioned(
            bottom: 12,
            right: 12,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                StreamBuilder(
                  stream: _linesController.stream,
                  builder: (context, _) => IconButton(
                    icon: Icon(Icons.undo),
                    onPressed: _latestBackup.isEmpty ? null : () => _undo(),
                  ),
                ),
                Row(
                  children: [
                    ColorButton(value: 0XFFFBF120),
                    ColorButton(value: 0XFF2EC627),
                    ColorButton(value: 0XFF337AFF),
                    ColorButton(value: 0XFF900C3F),
                    ColorButton(value: 0XFFFF5733),
                    StatefulBuilder(
                      builder: (context, newState) => IconButton(
                        icon: _savingNote
                            ? SizedBox(
                                width: 22,
                                height: 22,
                                child: CircularProgressIndicator(),
                              )
                            : Icon(Icons.check),
                        onPressed: _savingNote
                            ? null
                            : () async {
                                if (_points.isNotEmpty) {
                                  newState(() => _savingNote = true);
                                  await _saveNote();
                                  Navigator.pop(context, true);
                                }
                              },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _linesController.close();
    _colorSubscription.cancel();
    ColorSelection.dispose();
    super.dispose();
  }
}
