import 'package:flutter/widgets.dart';
import 'dart:ui' as ui;

class ScribblePainter extends CustomPainter {
  final List<Offset> points;
  final int color;

  ScribblePainter(this.points, this.color);

  List<Offset> offsetPoints = <Offset>[];

  void paint(Canvas canvas, Size size) {
    final Paint _paint = Paint()
      ..color = Color(color)
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 1.6;
    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null)
        canvas.drawLine(points[i], points[i + 1], _paint);
      if (points[i] != null && points[i + 1] == null) {
        offsetPoints.clear();
        offsetPoints.add(points[i]);
        offsetPoints.add(Offset(points[i].dx + 0.1, points[i].dy + 0.1));
        canvas.drawPoints(ui.PointMode.points, offsetPoints, _paint);
      }
    }
  }

  bool shouldRepaint(ScribblePainter other) => other.points != points;
}
