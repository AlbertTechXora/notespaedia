import 'dart:async';
import 'package:flutter/material.dart';

import '../../../../../logic/local_db/local_db.dart';

class AddText extends StatefulWidget {
  final String documentName;
  final int page;
  final Offset initialOffset;

  AddText({
    @required this.documentName,
    @required this.page,
    @required this.initialOffset,
  });

  @override
  State<StatefulWidget> createState() {
    return _AddTextState();
  }
}

class _AddTextState extends State<AddText> {
  final TextEditingController _textController = TextEditingController();
  final StreamController _offsetController = StreamController.broadcast();

  Offset _currentOffset;

  StreamSubscription _offsetSubscription;

  @override
  void initState() {
    super.initState();
    _currentOffset = widget.initialOffset;
    _offsetSubscription = _offsetController.stream.listen(
      (newOffset) => _currentOffset = newOffset,
    );
  }

  bool _savingNote = false;

  Future<void> _saveNote() async {
    _savingNote = true;

    // Insert note into local storage
    await LocalDB.db.insert(
      'Notes',
      {
        'document_name': widget.documentName,
        'page': widget.page,
        'type': 'text',
        'text': _textController.text,
        'x': _currentOffset.dx,
        'y': _currentOffset.dy + 14,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white38,
      child: Stack(
        children: [
          StreamBuilder(
            stream: _offsetController.stream,
            builder: (context, offset) => Transform.translate(
              offset: _currentOffset,
              child: GestureDetector(
                child: SizedBox(
                  width:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.width * 0.67
                          : MediaQuery.of(context).size.height * 0.67,
                  child: TextField(
                    controller: _textController,
                    autofocus: true,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.all(0),
                    ),
                  ),
                ),
                onPanStart: (details) =>
                    _offsetController.add(details.globalPosition),
                onPanUpdate: (details) =>
                    _offsetController.add(details.globalPosition),
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: IconButton(
              icon: Icon(Icons.close),
              onPressed: () => Navigator.pop(context),
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).viewInsets.bottom,
            right: 0,
            child: StatefulBuilder(
              builder: (context, newState) => IconButton(
                icon: _savingNote
                    ? SizedBox(
                        width: 22,
                        height: 22,
                        child: CircularProgressIndicator(),
                      )
                    : Icon(Icons.check),
                onPressed: _savingNote
                    ? null
                    : () async {
                        if (_textController.text.isEmpty)
                          Navigator.pop(context);
                        else {
                          newState(() => _savingNote = true);
                          await _saveNote();
                          Navigator.pop(context, true);
                        }
                      },
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    _offsetController.close();
    _offsetSubscription.cancel();
    super.dispose();
  }
}
