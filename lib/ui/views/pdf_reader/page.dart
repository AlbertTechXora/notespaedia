import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';

import '../../../logic/local_db/local_db.dart';
import '../pdf_reader/editor/bloc/notes.dart';
import '../pdf_reader/editor/bloc/selected_layer.dart';
import '../pdf_reader/editor/scribble/scribble_painter.dart';

class BoundingBoxPainter extends CustomPainter {
  final Offset offset;
  final double width, height;

  BoundingBoxPainter(this.offset, this.width, this.height);

  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.grey
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.5;
    canvas.drawRect(offset & Size(width, height), paint);
  }

  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class PDFPage extends StatefulWidget {
  final PdfDocument document;
  final int pageNumber;
  final Function([bool]) showEditor;

  PDFPage({
    @required this.document,
    @required this.pageNumber,
    @required this.showEditor,
  });

  @override
  State<StatefulWidget> createState() {
    return _PDFPageState();
  }
}

class _PDFPageState extends State<PDFPage> {
  List<Map<String, dynamic>> _notes;

  Future<void> _getNotes() async {
    _notes = await LocalDB.db.rawQuery(
      'SELECT * FROM Notes '
      'WHERE document_name = "${widget.document.sourceName}" '
      'AND page = ${widget.pageNumber}',
    );
    return _notes;
  }

  Future<Uint8List> _getPageWithContents() async {
    // Get page image bytes
    final page = await widget.document.getPage(widget.pageNumber);
    final pageImage = await page.render(
      width: page.width,
      height: page.height,
      format: PdfPageFormat.PNG,
      backgroundColor: '#FFFFFF',
    );

    // Required by the plugin
    await page.close();

    final Uint8List image = pageImage.bytes;

    await LocalDB.db.rawUpdate(
      'UPDATE LibraryCache SET thumbnail = ? WHERE path = ?',
      [image, widget.document.sourceName.substring(6)],
    );

    return image;
  }

  Size _textSize(String text, [double fontSize]) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: TextStyle(fontSize: fontSize ?? 17)),
      maxLines: 1,
      textDirection: TextDirection.ltr,
    )..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: FutureBuilder(
        future: _getPageWithContents(),
        builder: (context, page) => page.connectionState == ConnectionState.done
            ? GestureDetector(
                child: InteractiveViewer(
                  minScale: 1,
                  panEnabled: false,
                  boundaryMargin: const EdgeInsets.all(100),
                  child: StreamBuilder(
                    stream: Notes.stream,
                    builder: (context, _) => FutureBuilder(
                      future: _getNotes(),
                      builder: (context, notes) {
                        return Stack(
                          children: [
                            Center(child: Image.memory(page.data)),
                            if (notes.hasData)
                              for (var note in notes.data)
                                note['type'] == 'scribble'
                                    ? Positioned(
                                        left: 0,
                                        right: 0,
                                        child: FittedBox(
                                          fit: BoxFit.fitWidth,
                                          child: Transform.scale(
                                            scale: 1,
                                            child: SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height -
                                                  20,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: CustomPaint(
                                                painter: ScribblePainter(
                                                  [
                                                    for (var offset
                                                        in jsonDecode(
                                                            note['points']))
                                                      offset == null
                                                          ? null
                                                          : Offset(
                                                              offset['x'],
                                                              offset['y'],
                                                            ),
                                                  ],
                                                  note['color'],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Positioned.fill(
                                        child: Transform.translate(
                                          offset: Offset(note['x'], note['y']),
                                          child: GestureDetector(
                                            child: DefaultTextStyle(
                                              style: const TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              child: Stack(
                                                children: <Widget>[
                                                  Text(
                                                    note['text'],
                                                    style: TextStyle(
                                                      foreground: Paint()
                                                        ..style =
                                                            PaintingStyle.stroke
                                                        ..color = Colors.white
                                                        ..strokeWidth = 1,
                                                    ),
                                                  ),
                                                  Text(note['text']),
                                                ],
                                              ),
                                            ),
                                            onTap: () {
                                              final Size _size =
                                                  _textSize(note['text']);
                                              SelectedLayer.change(
                                                {
                                                  'id': note['id'],
                                                  'type': note['type'],
                                                  'offset': Offset(
                                                    note['x'] - 8,
                                                    note['y'] - 8,
                                                  ),
                                                  'width': _size.width + 16,
                                                  'height': _size.height + 16,
                                                },
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                            StreamBuilder(
                              stream: SelectedLayer.stream,
                              builder: (context, boundingBox) =>
                                  boundingBox.hasData
                                      ? Positioned.fill(
                                          child: CustomPaint(
                                            painter: BoundingBoxPainter(
                                              boundingBox.data['offset'],
                                              boundingBox.data['width'],
                                              boundingBox.data['height'],
                                            ),
                                          ),
                                        )
                                      : const SizedBox(),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
                onTapDown: (details) {
                  SelectedLayer.change(null);
                  widget.showEditor();
                  int tappedNoteID;
                  String tappedNoteType;
                  double closestNoteDistance = double.infinity;
                  _notes.forEach(
                    (note) {
                      if (note['type'] == 'scribble')
                        for (var offset in jsonDecode(note['points']))
                          if (offset != null) {
                            final double _currentDistance =
                                (Offset(offset['x'], offset['y']) -
                                        details.globalPosition)
                                    .distance;
                            if (_currentDistance < closestNoteDistance) {
                              closestNoteDistance = _currentDistance;
                              tappedNoteID = note['id'];
                              tappedNoteType = note['type'];
                            }
                          }
                    },
                  );
                  if (closestNoteDistance < 30) {
                    double left = double.infinity;
                    double top = double.infinity;
                    double right = 0;
                    double bottom = 0;

                    final Map selectedNote = _notes
                        .singleWhere((note) => note['id'] == tappedNoteID);

                    for (var offset in jsonDecode(selectedNote['points']))
                      if (offset != null) {
                        if (offset['x'] < left) left = offset['x'];
                        if (offset['x'] > right) right = offset['x'];
                        if (offset['y'] < top) top = offset['y'];
                        if (offset['y'] > bottom) bottom = offset['y'];
                      }

                    final Offset offset = Offset(left - 12, top - 12);
                    final double width = right - left + 24;
                    final double height = bottom - top + 24;

                    SelectedLayer.change(
                      {
                        'id': tappedNoteID,
                        'type': tappedNoteType,
                        'offset': offset,
                        'width': width,
                        'height': height
                      },
                    );
                  }
                },
              )
            : const SizedBox(),
      ),
    );
  }
}
