import 'dart:typed_data';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';

import '../../../logic/local_db/local_db.dart';
import '../pdf_reader/editor/bloc/notes.dart';
import '../pdf_reader/editor/bloc/page_number.dart';
import '../pdf_reader/editor/bloc/selected_layer.dart';
import '../pdf_reader/editor/display/editor_display.dart';
import '../pdf_reader/editor/layer_options.dart';
import '../pdf_reader/page.dart';

class PDFReader extends StatefulWidget {
  final String path;

  PDFReader({@required this.path});

  @override
  State<StatefulWidget> createState() {
    return _PDFReaderState();
  }
}

class _PDFReaderState extends State<PDFReader> {
  static int _pageIndex = 0;

  PageController _pageController;

  Future<PdfDocument> _getDocument() async {
    debugPrint("widget.path: ${widget.path}");
    final PdfDocument _document = await PdfDocument.openAsset(widget.path);

    List<Map> documentCache = await LocalDB.db.rawQuery(
      'SELECT * FROM Documents '
      'WHERE name = "${_document.sourceName}" '
      'LIMIT 1',
    );

    if (documentCache.isNotEmpty)
      _pageIndex = documentCache[0]['current_page'];
    else
      await LocalDB.db.rawInsert(
        'INSERT INTO Documents(name, current_page) '
        'VALUES("${_document.sourceName}", 0)',
      );

    _pageController = PageController(initialPage: _pageIndex);

    _pageController.addListener(() async {
      if (_pageController.page.round() != _pageIndex) {
        _pageIndex = _pageController.page.round();
        PageNumber.change(_pageIndex + 1);
        if (_editorVisible) _editorViewController.add(true);
        await LocalDB.db.rawUpdate(
          'UPDATE Documents SET current_page = ? WHERE name = ?',
          [_pageIndex, _document.sourceName],
        );
      }
    });

    return _document;
  }

  bool _editorVisible = false;

  final StreamController<bool> _editorViewController =
      StreamController.broadcast();

  void _showEditor([bool visible]) {
    _editorVisible = visible ?? !_editorVisible;
    _editorViewController.add(_editorVisible);
  }

  @override
  void initState() {
    super.initState();
    Notes.init();
    PageNumber.init();
    SelectedLayer.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      resizeToAvoidBottomInset: false,
      body: FutureBuilder(
        future: _getDocument(),
        builder: (context, document) => document.hasError
            ? Center(
                child: Text(
                  document.error.toString(),
                  textAlign: TextAlign.center,
                ),
              )
            : document.connectionState == ConnectionState.done
                ? SafeArea(
                    child: Stack(
                      children: [
                        PageView.builder(
                          controller: _pageController,
                          scrollDirection: Axis.vertical,
                          itemCount: document.data.pagesCount,
                          itemBuilder: (context, index) => PDFPage(
                            document: document.data,
                            pageNumber: index + 1,
                            showEditor: _showEditor,
                          ),
                        ),
                        StreamBuilder(
                          stream: SelectedLayer.stream,
                          builder: (context, selected) => selected.hasData
                              ? LayerOptions(
                                  id: selected.data['id'],
                                  type: selected.data['type'],
                                )
                              : StreamBuilder(
                                  stream: _editorViewController.stream,
                                  initialData: _editorVisible,
                                  builder: (context, visible) => visible.data
                                      ? EditorOptions(
                                          showEditor: _showEditor,
                                          documentName:
                                              document.data.sourceName,
                                          pageCount: document.data.pagesCount,
                                        )
                                      : const SizedBox(),
                                ),
                        ),
                      ],
                    ),
                  )
                : Center(
                    child: SizedBox(
                      height: 64,
                      width: 64,
                      child: CircularProgressIndicator(),
                    ),
                  ),
      ),
    );
  }

  @override
  void dispose() {
    Notes.dispose();
    PageNumber.dispose();
    SelectedLayer.dispose();
    _editorViewController.close();
    super.dispose();
  }
}
