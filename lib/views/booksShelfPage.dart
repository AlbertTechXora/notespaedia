import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myNavDrawer.dart';
import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/topDesignWave.dart';
import '../widgets/bookCategories.dart';
import '../widgets/myFloatingButton.dart';

class BooksShelfPage extends StatefulWidget {
  @override
  _BooksShelfPageState createState() => _BooksShelfPageState();
}

class _BooksShelfPageState extends State<BooksShelfPage> {
  var maxWidth;
  List<Map<String, String>> categoriesOnTop = [
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
  ];
  List<Map<String, String>> categoriesOnBottom = [
    {
      "image": "assets/images/heartRating.png",
      "title": "Ear",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Eye",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Biochemistry",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Ear",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Eye",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Biochemistry",
    },
  ];
  List<Map<String, String>> freshRelease = [
    {
      "image": "assets/images/eliteanatomy.jpg",
      "title": "Elite Anatomy",
    },
    {
      "image": "assets/images/endocrinology.jpg",
      "title": "Endocrinology",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
      "title": "Respiratory Medicine",
    },
    {
      "image": "assets/images/neuronotes.jpg",
      "title": "Neuronotes",
    },
    {
      "image": "assets/images/eliteanatomy.jpg",
      "title": "Elite Anatomy",
    },
    {
      "image": "assets/images/endocrinology.jpg",
      "title": "Endocrinology",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
      "title": "Respiratory Medicine",
    },
    {
      "image": "assets/images/neuronotes.jpg",
      "title": "Neuronotes",
    },
  ];

  Widget _buildItemsList() {
    Widget itemCards;

    // var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    // final double itemWidth = size.width / 2;

    if (freshRelease.length > 0) {
      itemCards = GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        // childAspectRatio: (itemWidth / itemHeight),
        // childAspectRatio: 0.64,
        scrollDirection: Axis.vertical,
        crossAxisCount: 3,
        childAspectRatio: 0.60,
        padding: const EdgeInsets.all(6.0),
        crossAxisSpacing: 5.0,
        children: List.generate(freshRelease.length, (index) {
          return GestureDetector(
            child: Container(
              child: Column(
                children: [
                  Card(
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    child: Container(
                      width: maxWidth > 600 ? 250 : 90.0,
                      height: maxWidth > 600 ? 380 : 130.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18.0),
                        image: DecorationImage(
                          image: AssetImage(
                            freshRelease[index]['image'],
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Flexible(
                    child: Text(
                      freshRelease[index]['title'],
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            onTap: () {
              changeBookNumsRead();
            },
          );
        }),
      );
    } else {
      itemCards = Container(
        child: Text('Shelf is Empty.'),
      );
    }
    return itemCards;
  }

  changeBookNumsRead() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<int> numBooksReadToday =
        preferences.getInt('numBooksReadToday', defaultValue: 0);
    var sharedNumBooksReadToday = numBooksReadToday.getValue();
    preferences.setInt('numBooksReadToday', sharedNumBooksReadToday + 1);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        maxWidth = constraints.maxWidth;
        return Scaffold(
          appBar: MyAppBar(),
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          body: Container(
            child: ListView(
              children: [
                TopDesignWave("Shelf"),
                BookCategories(
                  categoriesOnTop: categoriesOnTop,
                  categoriesOnBottom: categoriesOnBottom,
                ),
                Container(
                  padding: EdgeInsets.all(8.0),
                  margin: EdgeInsets.only(
                    top: 15.0,
                    bottom: 15.0,
                  ),
                  child: _buildItemsList(),
                ),
              ],
            ),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
