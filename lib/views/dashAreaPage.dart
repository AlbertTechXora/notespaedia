import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'package:sqflite/sqflite.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../models/connections.dart' as Constants;

import '../widgets/myTimer.dart';
import '../widgets/myAppBar.dart';
import '../widgets/topDesignWave.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';
import '../widgets/myNavDrawer.dart';
import '../views/timeTablePage.dart';

class DashAreaPage extends StatefulWidget {
  @override
  _DashAreaPageState createState() => _DashAreaPageState();
}

class _DashAreaPageState extends State<DashAreaPage> {
  bool isDarkMode = false;
  List<DataRow> _rowList = [];
  List _graphGoalHrsList = [];
  List _dailyGoalsWeekList = [];
  bool loaded = false;
  var sharedDailyGoalHrs = 0.0;
  var sharedBooksReadOnToday = 0;

  List _daysInaWeek = ["M", "T", "W", "T", "F", "S", "S"];
  var now = new DateTime.now();

  BezierChartScale bezierChartScale = BezierChartScale.CUSTOM;

  String percentageModifier(double value) {
    final roundedValue = value.ceil().toInt().toString();
    return '$roundedValue';
  }

  List<Widget> appBarActions = [
    Builder(
      builder: (context) => InkWell(
        child: Center(
          child: Container(
            height: 40.0,
            width: 40.0,
            margin: EdgeInsets.only(right: 8.0),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Image.asset(
                "assets/icons/userDefault.png",
                height: 25.0,
                width: 25.0,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
        onTap: () {
          print("Clicked");
          Scaffold.of(context).openEndDrawer();
        },
      ),
    ),
  ];

  Widget weeklyChart(BuildContext context, bezierChartScale) {
    // print("bezierChartScale: $bezierChartScale");

    int currentDay = now.weekday;
    DateTime firstDayOfWeek = now.subtract(Duration(days: currentDay - 1));
    print("firstDayOfWeek: $firstDayOfWeek");
    // print("DayFromDate: ${firstDayOfWeek.day}");
    // print("MonthFromDate: ${firstDayOfWeek.month}");
    // print("YearFromDate: ${firstDayOfWeek.year}");
    final fromDate =
        DateTime(firstDayOfWeek.year, firstDayOfWeek.month, firstDayOfWeek.day);
    final toDate = DateTime.now();

    final dateToday = DateTime.now().subtract(Duration(days: 0));
    final date0 = DateTime.now().subtract(Duration(days: 1));
    final date1 = DateTime.now().subtract(Duration(days: 2));
    final date2 = DateTime.now().subtract(Duration(days: 3));
    final date3 = DateTime.now().subtract(Duration(days: 4));
    final date4 = DateTime.now().subtract(Duration(days: 5));
    final date5 = DateTime.now().subtract(Duration(days: 6));

    return bezierChartScale == BezierChartScale.CUSTOM
        ? BezierChart(
            bezierChartScale: BezierChartScale.CUSTOM,
            xAxisCustomValues: const [0, 10],
            series: [
              BezierLine(
                dataPointStrokeColor: Colors.white,
                lineColor: Theme.of(context).primaryColor,
                label: "h",
                data: [
                  DataPoint<double>(value: 0, xAxis: 0),
                  DataPoint<double>(value: sharedDailyGoalHrs, xAxis: 10),
                ],
              ),
            ],
            config: BezierChartConfig(
              verticalIndicatorStrokeWidth: 3.0,
              verticalIndicatorColor: Colors.black26,
              showVerticalIndicator: true,
              verticalIndicatorFixedPosition: false,
              backgroundColor: HexColor("#8B82F9"),
              footerHeight: 40.0,
            ),
          )
        : BezierChart(
            fromDate: fromDate,
            bezierChartScale: bezierChartScale,
            toDate: toDate,
            selectedDate: toDate,
            series: [
              BezierLine(
                dataPointStrokeColor: Colors.white,
                lineColor: Theme.of(context).primaryColor,
                label: "h",
                onMissingValue: (dateTime) {
                  // if (dateTime.day.isEven) {
                  //   return 3.0;
                  // }
                  return 0.0;
                },
                data: [
                  // DataPoint<DateTime>(value: 6, xAxis: date0),
                  // DataPoint<DateTime>(value: 10, xAxis: date1),
                  // DataPoint<DateTime>(value: 15, xAxis: date2),
                  // DataPoint<DateTime>(value: 5, xAxis: date3),
                  // DataPoint<DateTime>(value: 12, xAxis: date4),
                  // DataPoint<DateTime>(value: 9, xAxis: date5),
                  // DataPoint<DateTime>(value: 8, xAxis: dateToday),

                  if (_graphGoalHrsList.isNotEmpty)
                    // {
                    for (var values in _graphGoalHrsList)
                      //       {
                      DataPoint<DateTime>(
                          value: values['DailyGoalsHrs'],
                          xAxis: values['WeekDay'] == currentDay
                              ? dateToday
                              : values['WeekDay'] == currentDay - 1
                                  ? date0
                                  : values['WeekDay'] == currentDay - 2
                                      ? date1
                                      : values['WeekDay'] == currentDay - 3
                                          ? date2
                                          : values['WeekDay'] == currentDay - 4
                                              ? date3
                                              : values['WeekDay'] ==
                                                      currentDay - 5
                                                  ? date4
                                                  : date5),
                  //       }
                  // }
                ],
              ),
            ],
            config: BezierChartConfig(
              verticalIndicatorStrokeWidth: 3.0,
              verticalIndicatorColor: Colors.black26,
              showVerticalIndicator: true,
              verticalIndicatorFixedPosition: false,
              backgroundColor: HexColor("#8B82F9"),
              footerHeight: 40.0,
            ),
          );
  }

  addDatatableRows() {
    setState(() {
      for (var i = 0; i < _daysInaWeek.length; i++) {
        _rowList.add(
          DataRow(
            cells: <DataCell>[
              DataCell(
                Text(
                  _daysInaWeek[i],
                  style: TextStyle(
                    color: HexColor("#002231"),
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                  ),
                  // style: Theme.of(context).textTheme.subtitle2,
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#C5ECEB"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'P',
                        style: TextStyle(
                          color: HexColor("#31DAB7"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#FFD3D4"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'A',
                        style: TextStyle(
                          color: HexColor("#FD6866"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#FFCDF3"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'B',
                        style: TextStyle(
                          color: HexColor("#FE51C9"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#D8D3FD"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'P',
                        style: TextStyle(
                          color: HexColor("#786BF8"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#C5ECEB"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'P',
                        style: TextStyle(
                          color: HexColor("#31DAB7"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#FFD3D4"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'A',
                        style: TextStyle(
                          color: HexColor("#FD6866"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#FFCDF3"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'B',
                        style: TextStyle(
                          color: HexColor("#FE51C9"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
              DataCell(
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => TimeTablePage(),
                      ),
                    );
                  },
                  child: Container(
                    width: 32.0,
                    height: 20.0,
                    decoration: BoxDecoration(
                      color: HexColor("#D8D3FD"),
                      // color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Text(
                        'P',
                        style: TextStyle(
                          color: HexColor("#786BF8"),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),
                        // style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }
      loaded = true;
    });
  }

  getDailyGoalHrsAndBooksReadnSharedPrefs() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    Preference<double> dailyGoalHrs =
        preferences.getDouble('dailyGoalHrs', defaultValue: 0.0);
    dailyGoalHrs.listen((value) {
      setState(() {
        sharedDailyGoalHrs = value;
      });
    });
    // print("sharedDailyGoalHrs: $sharedDailyGoalHrs");
    Preference<int> numBooksReadToday =
        preferences.getInt('numBooksReadToday', defaultValue: 0);
    var sharedNumBooksReadToday = numBooksReadToday.getValue();
    setState(() {
      sharedBooksReadOnToday = sharedNumBooksReadToday;
    });
  }

  _getWeekDayGoalHrsFromDB(wkDay) async {
    print("Starting _getWeekDayGoalHrsFromDB...");
    print("wkDay:$wkDay");
    Database db = await openDatabase(Constants.dbName);
    String dailyGoalsTable = Constants.dailyGoalsTbl;
    List<Map> dailyGoalsTableExist = await db.rawQuery(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='$dailyGoalsTable'");
    if (dailyGoalsTableExist.length > 0) {
      // Get the records
      List<Map> dailyGoalsTableList = await db.rawQuery(
          'SELECT * FROM $dailyGoalsTable WHERE WeekDay=$wkDay AND Month=${now.month} AND Year=${now.year}');
      // print(list);
      var dailyGoalsTableListLength = dailyGoalsTableList.length;
      print("dailyGoalsTableListLength: $dailyGoalsTableListLength");
      if (dailyGoalsTableListLength > 0) {
        print("dailyGoalsTableList:$dailyGoalsTableList");
        for (var values in dailyGoalsTableList) {
          var dbDailyGoalsHrs = values['DailyGoalsHrs'].toDouble();
          setState(() {
            sharedDailyGoalHrs = dbDailyGoalsHrs;
          });
        }
      } else {
        print("Empty");
        setState(() {
          sharedDailyGoalHrs = 0;
        });
      }
    } else {
      print("Table doesnot exist: $dailyGoalsTable");
    }
  }

  _getWeekGoalHrsFromDBForGraph() async {
    print("Starting _getWeekGoalHrsFromDBForGraph...");
    Database db = await openDatabase(Constants.dbName);
    String dailyGoalsTable = Constants.dailyGoalsTbl;
    List<Map> dailyGoalsTableExist = await db.rawQuery(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='$dailyGoalsTable'");
    if (dailyGoalsTableExist.length > 0) {
      // Get the records
      List<Map> weekGoalHrsList = await db.rawQuery(
          'SELECT * FROM $dailyGoalsTable WHERE Month=${now.month} AND Year=${now.year}');
      // print(list);
      var weekGoalHrsListLength = weekGoalHrsList.length;
      print("weekGoalHrsListLength: $weekGoalHrsListLength");
      if (weekGoalHrsListLength > 0) {
        print("weekGoalHrsList:$weekGoalHrsList");
        setState(() {
          _graphGoalHrsList = weekGoalHrsList;
        });
        for (var values in weekGoalHrsList) {
          var tempDailyGoalWeekMap = {
            'day': values['WeekDay'],
            'weekDayColor': values['DailyGoalsHrs'] < 3 ? "red" : "green",
          };
          _dailyGoalsWeekList.add(tempDailyGoalWeekMap);
        }
        print("_dailyGoalsWeekList:$_dailyGoalsWeekList");
      } else {
        print("Empty");
      }
    } else {
      print("Table doesnot exist: $dailyGoalsTable");
    }
  }

  @override
  void initState() {
    super.initState();
    addDatatableRows();
    getDailyGoalHrsAndBooksReadnSharedPrefs();
    _getWeekGoalHrsFromDBForGraph();
    // print("nowWeekDay:${now.weekday}");
    // print("nowMonth:${now.month}");
    // print("nowYear:${now.year}");
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      var maxWidth = constraints.maxWidth;
      return Scaffold(
        // backgroundColor: HexColor("#FFF6F3"),
        appBar: MyAppBar(
          actions: appBarActions,
        ),
        endDrawer: Drawer(
          child: MyNavDrawer(),
        ),
        body: Container(
          child: ListView(
            children: [
              TopDesignWave("Dash"),
              Container(
                margin: EdgeInsets.only(bottom: 28.0),
                child: Column(
                  children: [
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.08,
                      width: maxWidth > 600
                          ? MediaQuery.of(context).size.width * 0.75
                          : MediaQuery.of(context).size.width * 0.95,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            HexColor("#831CD8"),
                            HexColor("#CF52D2"),
                          ],
                        ),
                      ),
                      child: Center(
                        child: MyTimer(),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      child: Container(
                        // height: MediaQuery.of(context).size.height * 0.06,
                        width: maxWidth > 600
                            ? MediaQuery.of(context).size.width * 0.45
                            : MediaQuery.of(context).size.width * 0.65,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: EdgeInsets.only(left: 10.0),
                        child: Row(
                          children: [
                            Container(
                              height: 15.0,
                              width: 15.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: Colors.purple,
                                  width: 4.0,
                                ),
                              ),
                              margin: EdgeInsets.only(right: 30.0),
                            ),
                            Text(
                              "INI CET 2020",
                              style: TextStyle(
                                color: Colors.purple,
                                fontSize: 20.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        // height: MediaQuery.of(context).size.height * 0.50,
                        width: MediaQuery.of(context).size.width * 0.95,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 15.0,
                                  width: 15.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: Colors.green,
                                      width: 4.0,
                                    ),
                                  ),
                                  margin: EdgeInsets.only(right: 5.0),
                                ),
                                Text(
                                  "Daily goals",
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Stack(
                              children: [
                                SleekCircularSlider(
                                  initialValue: sharedDailyGoalHrs,
                                  max: 24.0,
                                  appearance: CircularSliderAppearance(
                                    angleRange: 360,
                                    infoProperties: InfoProperties(
                                      modifier: percentageModifier,
                                      bottomLabelText: "books",
                                      mainLabelStyle: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 40.0,
                                        color: HexColor("#002231"),
                                      ),
                                      bottomLabelStyle: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    customColors: CustomSliderColors(
                                      trackColor: isDarkMode
                                          ? Colors.black
                                          : Colors.grey,
                                      progressBarColors: [
                                        HexColor("#93EE87"),
                                        HexColor("#1CD035"),
                                      ],
                                    ),
                                  ),
                                  onChange: (double value) {
                                    print(value);
                                  },
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      // color: Colors.transparent,
                                      // shape: BoxShape.circle,
                                      ),
                                  height: 150.0,
                                  width: 150.0,
                                ),
                                Positioned(
                                  top: 35.0,
                                  left: 20.0,
                                  child: Container(
                                    width: 110.0,
                                    decoration: BoxDecoration(
                                      color: isDarkMode
                                          ? Colors.grey
                                          : Colors.white.withOpacity(1.0),
                                      // color: Colors.red.withOpacity(1.0),
                                      shape: BoxShape.circle,
                                    ),
                                    child: Column(
                                      children: [
                                        Container(
                                          child: Column(
                                            children: [
                                              Text(
                                                "$sharedBooksReadOnToday",
                                                style: TextStyle(
                                                  color: HexColor("#002231"),
                                                  fontSize: 45.0,
                                                  fontWeight: FontWeight.bold,
                                                  height: 0.45,
                                                ),
                                              ),
                                              Text("books"),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5.0,
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Icon(Icons.timer),
                                              Text(
                                                "$sharedDailyGoalHrs" + "h",
                                                style: TextStyle(
                                                  color: HexColor("#002231"),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                "Adjust Goals",
                                style: TextStyle(color: Colors.black),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                  Colors.grey[300],
                                ),
                                shape:
                                    MaterialStateProperty.all<OutlinedBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                ),
                                minimumSize: MaterialStateProperty.all<Size>(
                                    Size(160.0, 38.0)),
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                for (var i = 0;
                                    i < _dailyGoalsWeekList.length;
                                    i++)
                                  GestureDetector(
                                    onTap: i >= now.weekday
                                        ? null
                                        : () {
                                            _getWeekDayGoalHrsFromDB(i + 1);
                                          },
                                    child: Container(
                                      height: 28.0,
                                      width: 28.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: i >= now.weekday
                                              ? Colors.grey[300]
                                              : Colors.green,
                                          width: 3.0,
                                        ),
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            _daysInaWeek[i],
                                            style: TextStyle(
                                              color: i >= now.weekday
                                                  ? Colors.grey[300]
                                                  : Colors.green,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        // height: MediaQuery.of(context).size.height * 0.45,
                        width: MediaQuery.of(context).size.width * 0.95,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: EdgeInsets.all(18.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 15.0,
                                  width: 15.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: HexColor("#8B82F9"),
                                      width: 4.0,
                                    ),
                                  ),
                                  margin: EdgeInsets.only(right: 5.0),
                                ),
                                Text(
                                  "Reading Stats",
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                            SizedBox(height: 10.0),
                            // weeklyChart(context, bezierChartScale),
                            Container(
                              // height: MediaQuery.of(context).size.height / 3,
                              // width: MediaQuery.of(context).size.width,
                              height: 250.0,
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: HexColor("#8B82F9"),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Stack(
                                children: [
                                  weeklyChart(context, bezierChartScale),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      SizedBox(
                                        width: 90.0,
                                        child: TextButton(
                                          onPressed: () {
                                            print(
                                                "Clicked Graph Format: DAILY");
                                            setState(() {
                                              bezierChartScale =
                                                  BezierChartScale.CUSTOM;
                                            });
                                          },
                                          child: Text(
                                            "DAILY",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 90.0,
                                        child: TextButton(
                                          onPressed: () {
                                            print(
                                                "Clicked Graph Format: WEEKLY");
                                            _getWeekGoalHrsFromDBForGraph();
                                            setState(() {
                                              bezierChartScale =
                                                  BezierChartScale.WEEKLY;
                                            });
                                          },
                                          child: Text(
                                            "WEEK",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 90.0,
                                        child: TextButton(
                                          onPressed: () {
                                            print(
                                                "Clicked Graph Format: MONTHLY");
                                            setState(() {
                                              bezierChartScale =
                                                  BezierChartScale.MONTHLY;
                                            });
                                          },
                                          child: Text(
                                            "MONTH",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.95,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: EdgeInsets.all(18.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 15.0,
                                  width: 15.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: HexColor("#FB6E03"),
                                      width: 4.0,
                                    ),
                                  ),
                                  margin: EdgeInsets.only(right: 5.0),
                                ),
                                Text(
                                  "Timetable",
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Container(
                              width: maxWidth > 600
                                  ? MediaQuery.of(context).size.width
                                  : null,
                              child: Theme(
                                data: Theme.of(context)
                                    .copyWith(dividerColor: Colors.white),
                                child: DataTable(
                                  columnSpacing: 5.0,
                                  horizontalMargin: 0.0,
                                  dataRowHeight: 35.0,
                                  columns: [
                                    DataColumn(label: Text('D/T')),
                                    DataColumn(label: Text('6am')),
                                    DataColumn(label: Text('9am')),
                                    DataColumn(label: Text('12pm')),
                                    DataColumn(label: Text('3pm')),
                                    DataColumn(label: Text('6pm')),
                                    DataColumn(label: Text('9pm')),
                                    DataColumn(label: Text('12am')),
                                    DataColumn(label: Text('3am')),
                                  ],
                                  rows: loaded ? _rowList : null,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: MyBottomNavBar(),
        floatingActionButton: MyFloatingButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      );
    });
  }
}
