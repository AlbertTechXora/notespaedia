import 'package:flutter/material.dart';
// import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

// import '../widgets/myNavDrawer.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  var counterVal = 0;

  void _incrementCounter() async {
    final preferences = await StreamingSharedPreferences.instance;
    setState(() {
      _counter++;
    });
    preferences.setInt('counter', _counter);
    Preference<int> counter = preferences.getInt('counter', defaultValue: 0);
    counter.listen((value) {
      counterVal = value;
    });
  }

  // void changeDarkOrLightMode() async {
  //   DynamicTheme.of(context).setBrightness(
  //       Theme.of(context).brightness == Brightness.dark
  //           ? Brightness.light
  //           : Brightness.dark);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Row(
            children: [
              Text(counterVal.toString()),
            ],
          ),
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.wb_sunny),
          )
        ],
      ),
      // endDrawer: Drawer(
      //   child: MyNavDrawer(),
      // ),
      // endDrawerEnableOpenDragGesture: true,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
