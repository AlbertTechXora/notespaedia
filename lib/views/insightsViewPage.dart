import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class InsightsViewPage extends StatefulWidget {
  @override
  _InsightsViewPageState createState() => _InsightsViewPageState();
}

class _InsightsViewPageState extends State<InsightsViewPage> {
  double linearPIPercent = 0.0;
  Timer _insightsTimer;
  bool isDarkMode = false;

  void startTimer() {
    _insightsTimer = Timer.periodic(Duration(milliseconds: 5), (timer) {
      setState(() {
        linearPIPercent += 0.001;
        if (linearPIPercent > 1) {
          _insightsTimer.cancel();
          Navigator.pop(context);
        }
      });
    });
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    startTimer();
    getSharedDataPref();
  }

  @override
  void dispose() {
    _insightsTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          GestureDetector(
            onTap: () {
              _insightsTimer.cancel();
              Navigator.pop(context);
            },
            onLongPress: () {
              _insightsTimer.cancel();
            },
            onLongPressUp: startTimer,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: HexColor("#6DAABE"),
              // color: isDarkMode
              //     ? Theme.of(context).primaryColor
              //     : HexColor("#6DAABE"),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 25.0,
                  ),
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                  SizedBox(
                    height: 85.0,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                    ),
                    child: Image.asset(
                      "assets/logos/NotespaediaInsightsLogo.jpg",
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                    ),
                    child: Row(
                      children: [
                        Text(
                          "Ectoderm layer of\neye has water\nrepelling feature\nwhich helps from\ndamages.",
                          style: TextStyle(
                            fontSize: 25.0,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 100.0,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 8.0, right: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              IconButton(
                                icon: Icon(
                                  Icons.share,
                                  color: Colors.white,
                                ),
                                onPressed: () {},
                              ),
                              IconButton(
                                icon: FaIcon(
                                  FontAwesomeIcons.instagram,
                                  color: Colors.white,
                                ),
                                onPressed: () {},
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5.0, right: 8.0),
                          child: SizedBox(
                            width: 140.0,
                            child: ElevatedButton(
                              onPressed: () {},
                              // elevation: 1.0,
                              child: Text(
                                "Go to notes",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              // color: HexColor("#7BB3C3"),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 36.0),
            child: LinearProgressIndicator(
              backgroundColor: Colors.grey,
              valueColor: AlwaysStoppedAnimation<Color>(
                Colors.white,
              ),
              value: linearPIPercent,
            ),
          )
        ],
      ),
    );
  }
}
