import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:sqflite/sqflite.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:path/path.dart' as Path;

import './notFound.dart';
import './insightsViewPage.dart';
import './descriptionPage.dart';
import './newUpdatesPage.dart';

import '../models/connections.dart' as Constants;

import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/topDesignWave.dart';
import '../widgets/bookCategories.dart';
import '../widgets/myFloatingButton.dart';
import '../widgets/myNavDrawer.dart';

class LibraryPage extends StatefulWidget {
  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  List<Map<String, String>> categoriesOnTop = [
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
    {
      "image": "assets/images/doctor.png",
      "title": "Anatomy",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Physiology",
    },
  ];
  List<Map<String, String>> categoriesOnBottom = [
    {
      "image": "assets/images/heartRating.png",
      "title": "Ear",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Eye",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Biochemistry",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Ear",
    },
    {
      "image": "assets/images/notes.png",
      "title": "Eye",
    },
    {
      "image": "assets/images/heartRating.png",
      "title": "Biochemistry",
    },
  ];
  List<Map<String, String>> freshRelease = [
    {
      "image": "assets/images/eliteanatomy.jpg",
      "title": "Elite Anatomy",
    },
    {
      "image": "assets/images/endocrinology.jpg",
      "title": "Endocrinology",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
      "title": "Respiratory Medicine",
    },
    {
      "image": "assets/images/eliteanatomy.jpg",
      "title": "Elite Anatomy",
    },
    {
      "image": "assets/images/endocrinology.jpg",
      "title": "Endocrinology",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
      "title": "Respiratory Medicine",
    },
  ];
  List<Map<String, String>> staffPicks = [
    {
      "image": "assets/images/lightedThree.png",
      "title": "for 3rd Year",
      "color": "#FB8B1A",
    },
    {
      "image": "assets/images/prizeStand.png",
      "title": "for Final Year",
      "color": "#F9B8F3",
    },
    {
      "image": "assets/images/lightedTwo.png",
      "title": "for 2nd Year",
      "color": "#7DD1AD",
    },
    {
      "image": "assets/images/lightedThree.png",
      "title": "for 3rd Year",
      "color": "#FB8B1A",
    },
    {
      "image": "assets/images/prizeStand.png",
      "title": "for Final Year",
      "color": "#F9B8F3",
    },
    {
      "image": "assets/images/lightedTwo.png",
      "title": "for 2nd Year",
      "color": "#7DD1AD",
    },
  ];
  List<Map<String, String>> bookSlidesonTop = [
    {
      "image": "assets/images/neuronotes.jpg",
    },
    {
      "image": "assets/images/endocrinology.jpg",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
    },
    {
      "image": "assets/images/neuronotes.jpg",
    },
    {
      "image": "assets/images/endocrinology.jpg",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
    },
  ];
  List<Map<String, String>> knowMoreSlides = [
    {
      "image": "assets/images/neuronotes.jpg",
      "title": "New\nUpdates",
      "color": "#117AD9",
      "value": "New Updates",
    },
    {
      "image": "assets/images/neuronotes.jpg",
      "title": "Coming\nSoon",
      "color": "#91C61D",
      "value": "Coming Soon",
    },
  ];

  setStreamSharedPref() async {
    print("Starting setStreamSharedPref...");
    final preferences = await StreamingSharedPreferences.instance;
    preferences.setInt('BottomAppbarItemIndex', 0);
  }

  _dbOperations() async {
    print("Starting _dbOperations...");
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = Path.join(databasesPath, Constants.dbName);
    print("databasesPath: $databasesPath");
    print("path: $path");

//     if (path != "") {
//       print("Deleting DB");
// // Delete the database
//       await deleteDatabase(path);
//     }
    String dailyGoalsTable = Constants.dailyGoalsTbl;
    // if (path == "") {
    // open the database
    Database database = await openDatabase(
      path, version: 1,
      onCreate: (Database db, int version) async {
        // When creating the db, create the table
        Batch batch = db.batch();
        batch.execute(
            "CREATE TABLE IF NOT EXISTS $dailyGoalsTable (DailyGoalsTableId INTEGER PRIMARY KEY AUTOINCREMENT, DailyGoalsHrs DOUBLE, WeekDay INTEGER, Month INTEGER, Year INTEGER);");

        List<dynamic> res = await batch.commit();
        print("BatchCommit:$res");
      },
      // onOpen: (Database db) async {
      //   List<Map> _custListFromDb =
      //       await db.rawQuery('SELECT * FROM $custTable');
      //   print("CustListFromDb:$_custListFromDb");
      // },
    );
    print("Database Created:$database");
    // }
    // _insertDummyHrs();
  }

//   _insertDummyHrs() async {
//     print("Starting _insertDummyHrs...");
//     // Get a location using getDatabasesPath
//     var databasesPath = await getDatabasesPath();
//     String path = Path.join(databasesPath, Constants.dbName);
//     String dailyGoalsTable = Constants.dailyGoalsTbl;

//     // open the database
//     Database database = await openDatabase(
//       path,
//       version: 1,
//     );
//     // Testing Only
//     // await database.execute('DELETE FROM $dailyGoalsTable');

// // Insert some records in a transaction
//     await database.transaction((txn) async {
//       int dailyGoalsTableId = await txn.rawInsert(
//           'INSERT INTO $dailyGoalsTable(DailyGoalsHrs, WeekDay, Month, Year) VALUES(10,1,2,2021)');
//       print('dailyGoalsTableId: $dailyGoalsTableId');
//     });
//   }

  @override
  void initState() {
    super.initState();
    _dbOperations();
    setStreamSharedPref();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return Scaffold(
          appBar: MyAppBar(),
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          body: Container(
            child: ListView(
              children: [
                TopDesignWave("Library"),
                Container(
                  padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height * 0.18,
                        height: maxWidth > 600 ? 150.0 : 110.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: bookSlidesonTop.length,
                          itemBuilder: (BuildContext context, index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => InsightsViewPage(),
                                  ),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.all(5.0),
                                padding:
                                    EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                                // height: MediaQuery.of(context).size.height * 0.18,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30.0),
                                  border: Border.all(
                                    color: Theme.of(context).primaryColor,
                                    width: 2,
                                  ),
                                ),
                                child: Container(
                                  // height: 110.0,
                                  width: maxWidth > 600 ? 105 : 65.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(18.0),
                                    image: DecorationImage(
                                      image: AssetImage(
                                          bookSlidesonTop[index]['image']),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Container(
                        // height: MediaQuery.of(context).size.height * 0.28,
                        height: maxWidth > 600 ? 240.0 : 180.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: knowMoreSlides.length,
                          itemBuilder: (BuildContext context, index) {
                            return InkWell(
                              onTap: knowMoreSlides[index]['value'] ==
                                      "New Updates"
                                  ? () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              NewUpdatesPage(),
                                        ),
                                      );
                                    }
                                  : () {},
                              child: Container(
                                margin: EdgeInsets.only(right: 15.0),
                                padding:
                                    EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                                // height: MediaQuery.of(context).size.height * 0.28,
                                width: maxWidth > 600
                                    ? MediaQuery.of(context).size.width * 0.50
                                    : MediaQuery.of(context).size.width * 0.93,
                                decoration: BoxDecoration(
                                  color:
                                      HexColor(knowMoreSlides[index]['color']),
                                  borderRadius: BorderRadius.circular(18.0),
                                ),
                                child: Container(
                                  padding: EdgeInsets.all(15.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            knowMoreSlides[index]['title'],
                                            style: TextStyle(
                                              fontSize: 28,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          ),
                                          Text(
                                            "Know more",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: RotationTransition(
                                          turns:
                                              AlwaysStoppedAnimation(12 / 360),
                                          child: Container(
                                            height: 250.0,
                                            width: 100.0,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(18.0),
                                              image: DecorationImage(
                                                image: AssetImage(
                                                    knowMoreSlides[index]
                                                        ['image']),
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Fresh Release",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                          SizedBox(
                            width: 80.0,
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text(
                                "more",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height * 0.35,
                        height: maxWidth > 600 ? 245.0 : 190.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: freshRelease.length,
                          itemBuilder: (BuildContext context, index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => DescriptionPage(
                                      image: freshRelease[index]['image'],
                                      title: freshRelease[index]['title'],
                                    ),
                                  ),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 10.0),
                                // width: MediaQuery.of(context).size.width * 0.35,
                                child: Column(
                                  children: [
                                    Card(
                                      elevation: 8.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ),
                                      child: Container(
                                        // width: MediaQuery.of(context).size.width *
                                        //     0.35,
                                        // height: MediaQuery.of(context).size.height *
                                        //     0.28,
                                        height: maxWidth > 600 ? 200.0 : 160.0,
                                        width: maxWidth > 600 ? 150.0 : 110.0,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          image: DecorationImage(
                                            image: AssetImage(
                                              freshRelease[index]['image'],
                                            ),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Flexible(
                                      child: Text(
                                        freshRelease[index]['title'],
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Categories",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      BookCategories(
                        categoriesOnTop: categoriesOnTop,
                        categoriesOnBottom: categoriesOnBottom,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Staff Picks",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 18.0),
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height * 0.30,
                        height: maxWidth > 600 ? 220.0 : 180.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: staffPicks.length,
                          itemBuilder: (BuildContext context, index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => NotFoundPage(),
                                  ),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 10.0),
                                // height: MediaQuery.of(context).size.height * 0.30,
                                // width: MediaQuery.of(context).size.width * 0.50,
                                width: maxWidth > 600 ? 240.0 : 200.0,
                                child: Card(
                                  color: HexColor(staffPicks[index]['color']),
                                  elevation: 2.0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  child: Stack(
                                    children: [
                                      Container(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Container(
                                              margin:
                                                  EdgeInsets.only(left: 45.0),
                                              height: 150.0,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                                image: DecorationImage(
                                                  image: AssetImage(
                                                    staffPicks[index]['image'],
                                                  ),
                                                  fit: BoxFit.contain,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        top: 15.0,
                                        left: 15.0,
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Top Picks",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline1,
                                              ),
                                              Text(
                                                staffPicks[index]['title'],
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline6,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
