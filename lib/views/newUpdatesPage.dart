import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myNavDrawer.dart';
import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';

class NewUpdatesPage extends StatefulWidget {
  @override
  _NewUpdatesPageState createState() => _NewUpdatesPageState();
}

class _NewUpdatesPageState extends State<NewUpdatesPage> {
  bool isDarkMode = false;
  List<Map<String, String>> newUpdatesList = [
    {
      "image": "assets/images/anaesthesiaPure.png",
      "title": "Elite Anaesthesia",
    },
    {
      "image": "assets/images/elitePaediatrics.png",
      "title": "Elite Paediatrics",
    },
  ];

  List newUpdatesPoints = [
    "New Updates on Corona Pandemic added",
    "Updated NICE Guidlines 2020 added",
    "Updated INICET 2020 questions added",
    "Updated NEET PG 2020 questions added"
  ];

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return Scaffold(
          appBar: MyAppBar(
            title: Text(
              "New Updates",
              style: TextStyle(color: Colors.white),
            ),
          ),
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemExtent: maxWidth > 600 ? 300.0 : 225.0,
              itemCount: newUpdatesList.length,
              itemBuilder: (BuildContext context, index) {
                return Stack(
                  children: [
                    Positioned(
                      top: 50.0,
                      left: 5.0,
                      right: 5.0,
                      child: Card(
                        elevation: 8.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        child: Container(
                          // height: MediaQuery.of(context).size.height * 0.26,
                          height: maxWidth > 600 ? 230.0 : 165.0,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            // color: Colors.white,
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          child: Container(
                            padding: maxWidth > 600
                                ? EdgeInsets.fromLTRB(200.0, 10.0, 5.0, 10.0)
                                : EdgeInsets.fromLTRB(135.0, 10.0, 5.0, 10.0),
                            // width: MediaQuery.of(context).size.width * 0.50,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  newUpdatesList[index]['title'],
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                for (var points in newUpdatesPoints)
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.circle,
                                        size: 3.0,
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Flexible(
                                        child: Text(
                                          points,
                                          style: TextStyle(
                                            color: isDarkMode
                                                ? Colors.black
                                                : null,
                                            fontSize: 10.0,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ],
                                  ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                Text(
                                  "Go to notes",
                                  style: Theme.of(context).textTheme.headline5,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 15.0,
                      top: 10.0,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        elevation: 6.0,
                        child: Container(
                          height: maxWidth > 600 ? 230.0 : 180.0,
                          width: maxWidth > 600 ? 180.0 : 120.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(18.0),
                            image: DecorationImage(
                              image: AssetImage(newUpdatesList[index]['image']),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
