import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myNavDrawer.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';

class NotFoundPage extends StatefulWidget {
  @override
  _NotFoundPageState createState() => _NotFoundPageState();
}

class _NotFoundPageState extends State<NotFoundPage> {
  bool isDarkMode = false;

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return Scaffold(
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                SizedBox(
                  height: maxWidth > 600 ? 240.0 : 120.0,
                ),
                Container(
                  height: maxWidth > 600 ? 500.0 : 250.0,
                  width: maxWidth > 600 ? 500.0 : 250.0,
                  child: Image.asset("assets/images/bookNototFound.png"),
                ),
                Text(
                  "Oops!",
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                    color: isDarkMode ? Colors.white : Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "This book is not available in our store",
                  style: TextStyle(
                    fontSize: 15,
                    color: isDarkMode ? Colors.white : Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Why don't you try a\ndiffrent book?",
                  style: TextStyle(
                    fontSize: 20,
                    color: isDarkMode
                        ? Theme.of(context).primaryColorLight
                        : Theme.of(context).primaryColor,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
