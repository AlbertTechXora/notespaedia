import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

import './signUpPage.dart';

class OnBoardingSreen extends StatefulWidget {
  @override
  _OnBoardingSreenState createState() => _OnBoardingSreenState();
}

class _OnBoardingSreenState extends State<OnBoardingSreen> {
  int currentOnBoardScreen = 0;

  PageController onBoardScreenPageViewCtrl = new PageController();
  List<Map<String, String>> onBoardData = [
    {
      "image": "assets/images/onBoardingPage1.png",
      "title": "Welcome to Notespaedia",
      "description":
          "Bring Med school on your tips, browse\nthrough our curated library to read\nmillions of handwritten notes"
    },
    {
      "image": "assets/images/onBoardingPage2.png",
      "title": "Read, annotate and highlight.",
      "description":
          "Customize the notes by adding extra\npoints which you feel are important."
    },
    {
      "image": "assets/images/onBoardingPage3.png",
      "title": "Track your progress",
      "description":
          "Set goals, assign reading hours, chart your\ntimetable and get notified for revision with\nour spaced repetition technology"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: PageView.builder(
                controller: onBoardScreenPageViewCtrl,
                onPageChanged: (value) {
                  setState(() {
                    currentOnBoardScreen = value;
                  });
                },
                itemCount: onBoardData.length,
                itemBuilder: (context, index) => OnBoardingContents(
                  image: onBoardData[index]['image'],
                  title: onBoardData[index]['title'],
                  description: onBoardData[index]['description'],
                  index: index,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: currentOnBoardScreen == 2
                  ? Column(
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) => SignUpPage(),
                              ),
                            );
                          },
                          child: Text(
                            "Let's Start",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                            onBoardData.length,
                            (index) => buildOnBoardDots(context, index),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(
                              // minWidth: 60,
                              onPressed: () {
                                onBoardScreenPageViewCtrl.animateToPage(
                                  2,
                                  duration: Duration(milliseconds: 300),
                                  curve: Curves.linear,
                                );
                              },
                              child: Text(
                                "Skip",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildOnBoardDots(BuildContext context, int index) {
    return AnimatedContainer(
      duration: kThemeAnimationDuration,
      height: 12,
      width: 12,
      margin: EdgeInsets.only(left: 15.0),
      decoration: BoxDecoration(
        color: currentOnBoardScreen == index
            ? Theme.of(context).primaryColor
            : HexColor("#787878"),
        borderRadius: BorderRadius.circular(6),
      ),
    );
  }
}

class OnBoardingContents extends StatelessWidget {
  final String image, title, description;
  final int index;
  const OnBoardingContents(
      {Key key, this.image, this.title, this.description, this.index});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(),
        Container(
          // margin: EdgeInsets.only(top: 50.0),
          padding: EdgeInsets.all(25.0),
          child: Image.asset(
            image,
            // height: 250.0,
            // width: 250.0,
            fit: BoxFit.contain,
          ),
        ),
        // Spacer(),
        SizedBox(
          height: 30.0,
        ),
        Text(
          title,
          style: index == 0
              ? Theme.of(context).textTheme.headline1
              : Theme.of(context).textTheme.headline6,
        ),
        SizedBox(
          height: 10.0,
        ),
        Text(
          description,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        // Spacer(),
        SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
