import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

import './libraryPage.dart';

class OtherDetailsScreen extends StatefulWidget {
  @override
  _OtherDetailsScreenState createState() => _OtherDetailsScreenState();
}

class _OtherDetailsScreenState extends State<OtherDetailsScreen> {
  int currentOnBoardScreen = 0;

  PageController otherDetailsScreenPageViewCtrl = new PageController();
  List<Map<String, String>> onBoardData = [
    {
      "title": "Please select your academic year",
      "description":
          "Bring Med school on your tips, browse\nthrough our curated library to read\nmillions of handwritten notes"
    },
    {
      "title": "Choose your medical college state",
      "description":
          "Customize the notes by adding extra\npoints which you feel are important."
    },
    {
      "title": "Choose your medical college",
      "description":
          "Set goals, assign reading hours, chart your\ntimetable and get notified for revision with\nour spaced repetition technology"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: HexColor("#4AC8C7"),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(
              height: 80.0,
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      onBoardData.length,
                      (index) => buildOnBoardDots(context, index),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: PageView.builder(
                controller: otherDetailsScreenPageViewCtrl,
                onPageChanged: (value) {
                  setState(() {
                    currentOnBoardScreen = value;
                  });
                },
                itemCount: onBoardData.length,
                itemBuilder: (context, index) => OtherDetailsContents(
                  title: onBoardData[index]['title'],
                  index: index,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildOnBoardDots(BuildContext context, int index) {
    return AnimatedContainer(
      duration: kThemeAnimationDuration,
      height: 8,
      width: 8,
      margin: EdgeInsets.only(left: 15.0),
      decoration: BoxDecoration(
        color:
            currentOnBoardScreen == index ? HexColor("#135F60") : Colors.white,
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }
}

class OtherDetailsContents extends StatelessWidget {
  final String title;
  final int index;
  const OtherDetailsContents({Key key, this.title, this.index});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.80,
          child: TextField(
            // controller: selectedDateCntlr,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(
                  width: 0,
                  style: BorderStyle.none,
                ),
              ),
              filled: true,
              contentPadding: EdgeInsets.all(16),
              fillColor: Colors.white,
            ),
          ),
        ),
        SizedBox(
          height: 100.0,
        ),
        index == 2
            ? ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => LibraryPage(),
                    ),
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(
                    color: HexColor("#002231"),
                  ),
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    Colors.white,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
