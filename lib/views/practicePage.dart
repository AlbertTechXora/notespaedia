import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../widgets/myNavDrawer.dart';
import '../widgets/topDesignWave.dart';
import '../widgets/myAppBar.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';

class PracticePage extends StatefulWidget {
  @override
  _PracticePageState createState() => _PracticePageState();
}

class _PracticePageState extends State<PracticePage>
    with SingleTickerProviderStateMixin {
  bool isDarkMode = false;
  TabController _controller;
  List<Map<String, String>> practiceRepsList = [
    {
      "image": "assets/images/endocrinology.jpg",
      "title": "Endocrinology",
      "subtitle": "Dr. Dilip Kumar",
      "time": "2d 3h",
      "order": "1st Read",
    },
    {
      "image": "assets/images/respiratoryMedicine.jpg",
      "title": "Respiratory Medicine",
      "subtitle": "DAMS 2020",
      "time": "7d 5h",
      "order": "2nd Read",
    },
  ];

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
    _controller = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return Scaffold(
          appBar: MyAppBar(),
          body: Container(
            child: ListView(
              children: [
                TopDesignWave("Practice"),
                Container(
                  child: TabBar(
                    isScrollable: true,
                    labelColor: isDarkMode
                        ? Colors.white
                        : Theme.of(context).primaryColor,
                    unselectedLabelColor: Colors.grey,
                    dragStartBehavior: DragStartBehavior.start,
                    indicatorColor: Theme.of(context).primaryColor,
                    controller: _controller,
                    tabs: [
                      Tab(
                        text: 'Reps',
                      ),
                      Tab(
                        text: 'Minmaps',
                      ),
                      Tab(
                        child: Row(
                          children: [Icon(Icons.search), Text("Bank")],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: maxWidth > 600
                      ? EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0)
                      : EdgeInsets.only(
                          top: 10.0,
                        ),
                  padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
                  height: MediaQuery.of(context).size.height * 0.70,
                  child: TabBarView(
                    controller: _controller,
                    children: <Widget>[
                      ListView(
                        children: [
                          Card(
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Container(
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Spaced Repetition",
                                        textAlign: TextAlign.start,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1,
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Text(
                                        "Get notified for your next\nnote to revise with our\nspaced repetition technology",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color:
                                              isDarkMode ? Colors.black : null,
                                        ),
                                      )
                                    ],
                                  ),
                                  Image.asset(
                                    "assets/images/brainBulb.png",
                                    height: 100.0,
                                    width: 100.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            // height: MediaQuery.of(context).size.height * 0.60,
                            margin: EdgeInsets.only(
                                top: maxWidth > 600 ? 20.0 : 10.0,
                                bottom: 30.0),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: practiceRepsList.length,
                              itemBuilder: (BuildContext context, index) {
                                return Card(
                                  elevation: 5.0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    child: Row(
                                      children: [
                                        Container(
                                          height: 190.0,
                                          width: 120.0,
                                          margin: EdgeInsets.fromLTRB(
                                              10.0, 10.0, 15.0, 10.0),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                practiceRepsList[index]
                                                    ['image'],
                                              ),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 185.0,
                                                child: Text(
                                                  practiceRepsList[index]
                                                      ['title'],
                                                  textAlign: TextAlign.start,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1,
                                                  overflow:
                                                      TextOverflow.visible,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5.0,
                                              ),
                                              Text(
                                                practiceRepsList[index]
                                                    ['subtitle'],
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: isDarkMode
                                                      ? Colors.black
                                                      : null,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 60.0,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.timer,
                                                    size: 15.0,
                                                    color: Colors.grey,
                                                  ),
                                                  Text(
                                                    practiceRepsList[index]
                                                        ['time'],
                                                    style: TextStyle(
                                                      color: isDarkMode
                                                          ? Colors.black
                                                          : null,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 20.0,
                                                  ),
                                                  Icon(
                                                    Icons.file_copy,
                                                    size: 15.0,
                                                    color: Colors.grey,
                                                  ),
                                                  Text(
                                                    practiceRepsList[index]
                                                        ['order'],
                                                    style: TextStyle(
                                                      color: isDarkMode
                                                          ? Colors.black
                                                          : null,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              TextButton(
                                                onPressed: () {},
                                                child: Text(
                                                  "Revise Now",
                                                  style: TextStyle(
                                                    color: isDarkMode
                                                        ? Colors.white
                                                        : Theme.of(context)
                                                            .primaryColor,
                                                  ),
                                                ),
                                                style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all<
                                                          Color>(
                                                    isDarkMode
                                                        ? Theme.of(context)
                                                            .primaryColor
                                                        : Theme.of(context)
                                                            .primaryColorLight,
                                                  ),
                                                  shape: MaterialStateProperty
                                                      .all<OutlinedBorder>(
                                                    RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0),
                                                    ),
                                                  ),
                                                  minimumSize:
                                                      MaterialStateProperty.all<
                                                              Size>(
                                                          Size(160.0, 38.0)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      ListView(
                        children: [
                          Card(
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Container(
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Spaced Repetition",
                                        textAlign: TextAlign.start,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1,
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Text(
                                        "Get notified for your next\nnote to revise with our\nspaced repetition technology",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color:
                                              isDarkMode ? Colors.black : null,
                                        ),
                                      )
                                    ],
                                  ),
                                  Image.asset(
                                    "assets/images/brainBulb.png",
                                    height: 100.0,
                                    width: 100.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      ListView(
                        children: [
                          Card(
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Container(
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Spaced Repetition",
                                        textAlign: TextAlign.start,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1,
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Text(
                                        "Get notified for your next\nnote to revise with our\nspaced repetition technology",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color:
                                              isDarkMode ? Colors.black : null,
                                        ),
                                      )
                                    ],
                                  ),
                                  Image.asset(
                                    "assets/images/brainBulb.png",
                                    height: 100.0,
                                    width: 100.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
