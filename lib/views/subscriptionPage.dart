import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

class SubscriptionPage extends StatefulWidget {
  @override
  _SubscriptionPageState createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  List<Map<String, String>> subscriptionList = [
    {
      "image": "assets/images/subscriptionTrophy.png",
      "title": "Doctor",
      "amount": "6999.0",
    },
    {
      "image": "assets/images/lightedOne.png",
      "title": "First Year",
      "amount": "1999.0",
    },
    {
      "image": "assets/images/lightedTwo.png",
      "title": "Second Year",
      "amount": "2999.0",
    },
  ];

  List subscriptionPoints = [
    "Unlimited access to all notes",
    "Spaced repetition",
    "Daily goal tracker",
    "Reading stats",
    "Countdown timer",
    "Timetable"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: Icon(
                    Icons.close,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
            Text(
              "Unlock\nUnlimited Access",
              style: Theme.of(context).textTheme.headline1,
            ),
            Text(
              "Choose your plan to get medicine on your tips",
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              height: MediaQuery.of(context).size.height * 0.70,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: subscriptionList.length,
                // physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, index) {
                  return Container(
                    padding: EdgeInsets.only(left: 10.0),
                    margin: EdgeInsets.only(bottom: 10.0),
                    // height: MediaQuery.of(context).size.height * 0.35,
                    height: 270.0,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          HexColor("#FF41BB"),
                          HexColor("#FF8D8D"),
                          HexColor("#FFBD6F"),
                        ],
                      ),
                    ),
                    child: Stack(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              // margin: EdgeInsets.only(left: 200),
                              height:
                                  subscriptionList[index]['title'] == "Doctor"
                                      ? 150.0
                                      : 185.0,
                              child: Image.asset(
                                // "assets/images/subscriptionTrophy.png",
                                subscriptionList[index]['image'],
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      subscriptionList[index]['title'],
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24.0,
                                        color: index % 2 == 0
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      subscriptionList[index]['amount'] + "/yr",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0,
                                        color: index % 2 == 0
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    for (var points in subscriptionPoints)
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.circle,
                                            size: 5.0,
                                            color: index % 2 == 0
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Flexible(
                                            child: Text(
                                              points,
                                              style: TextStyle(
                                                color: index % 2 == 0
                                                    ? Colors.white
                                                    : Colors.black,
                                                fontSize: 12.0,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      height: 38.0,
                                      width: 160.0,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        gradient: LinearGradient(
                                          begin: Alignment.centerLeft,
                                          end: Alignment.centerRight,
                                          colors: [
                                            HexColor("#831CD8"),
                                            HexColor("#CF52D2"),
                                          ],
                                        ),
                                      ),
                                      child: ElevatedButton(
                                        onPressed: () {},
                                        child: Text(
                                          "7 days free trial",
                                          style: TextStyle(
                                            color: HexColor("#FFC700"),
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        // color: Colors.transparent,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 5.0, top: 15.0, right: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Privacy Policy",
                    style: TextStyle(fontSize: 10.0),
                  ),
                  Text(
                    "Cancellation Policy",
                    style: TextStyle(fontSize: 10.0),
                  ),
                  Text(
                    "Restore Purchases",
                    style: TextStyle(fontSize: 10.0),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
