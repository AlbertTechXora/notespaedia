import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

import '../widgets/myNavDrawer.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';

class TimeTablePage extends StatefulWidget {
  @override
  _TimeTablePageState createState() => _TimeTablePageState();
}

class _TimeTablePageState extends State<TimeTablePage> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        return Scaffold(
          // backgroundColor: HexColor("#FFF6F3"),
          endDrawer: Drawer(
            child: MyNavDrawer(),
          ),
          body: Container(
            padding: EdgeInsets.all(10.0),
            child: ListView(
              children: [
                Row(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40.0),
                      ),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: EdgeInsets.all(5.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40.0),
                          ),
                          child: Row(
                            children: [
                              Icon(Icons.arrow_back_ios),
                              Text(
                                "Back",
                                style: Theme.of(context).textTheme.subtitle2,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              height: 15.0,
                              width: 15.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: HexColor("#FB6E03"),
                                  width: 4.0,
                                ),
                              ),
                              margin: EdgeInsets.only(right: 5.0),
                            ),
                            Text(
                              "Timetable",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                          ],
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ),
                          child: Container(
                            // width: MediaQuery.of(context).size.width / 2.5,
                            width: 150.0,
                            padding: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Icon(Icons.arrow_back_ios),
                                Text(
                                  "Wednesday",
                                  style: Theme.of(context).textTheme.subtitle2,
                                ),
                                Icon(Icons.arrow_forward_ios),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height * 0.70,
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: 10,
                            itemBuilder: (BuildContext context, index) {
                              return Container(
                                height: maxWidth > 600 ? 105.0 : 75.0,
                                child: Row(
                                  children: [
                                    Container(
                                      height: 70.0,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(2.0),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            child: Text("6:00 am"),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(2.0),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            child: Text("7:00 am"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Stack(
                                      children: [
                                        VerticalDivider(
                                          color: Colors.green,
                                          thickness: 2,
                                        ),
                                        index == 0
                                            ? Positioned(
                                                left: 5.0,
                                                child: Container(
                                                  color: Colors.white
                                                      .withOpacity(1.0),
                                                  height: 30.0,
                                                  width: 5.0,
                                                ),
                                              )
                                            : Container(),
                                        index == 10 - 1
                                            ? Positioned(
                                                left: 5.0,
                                                top: 30.0,
                                                child: Container(
                                                  color: Colors.white
                                                      .withOpacity(1.0),
                                                  height: 45.0,
                                                  width: 5.0,
                                                ),
                                              )
                                            : Container(),
                                        Positioned(
                                          left: 2.0,
                                          top: 30.0,
                                          child: Container(
                                            height: 12.0,
                                            width: 12.0,
                                            decoration: BoxDecoration(
                                              color: Colors.green,
                                              // color: Colors.green.withOpacity(1.0),
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      height: maxWidth > 600 ? 100.0 : 70.0,
                                      width: maxWidth > 600 ? 300 : 230.0,
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color:
                                            Theme.of(context).primaryColorLight,
                                      ),
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Column(
                                              children: [
                                                Text(
                                                  "P",
                                                  style: TextStyle(
                                                    color: Colors.purple,
                                                    fontSize: 32.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "Elite Paediatrics",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline4,
                                                ),
                                                Text("Paediatrics"),
                                              ],
                                            ),
                                            Container(
                                              child: RotationTransition(
                                                turns: AlwaysStoppedAnimation(
                                                    90 / 360),
                                                child: Icon(
                                                    Icons.arrow_forward_ios),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
          bottomNavigationBar: MyBottomNavBar(),
          floatingActionButton: MyFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}
