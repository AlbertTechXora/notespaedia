import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:bezier_chart/bezier_chart.dart';

import '../widgets/myTimer.dart';
import '../widgets/myAppBar.dart';
import '../widgets/topDesignWave.dart';
import '../widgets/myBottomNavBar.dart';
import '../widgets/myFloatingButton.dart';
import '../widgets/myNavDrawer.dart';
import '../widgets/mySubscriptionButton.dart';

class UnsubscribedDashArea extends StatefulWidget {
  @override
  _UnsubscribedDashAreaState createState() => _UnsubscribedDashAreaState();
}

class _UnsubscribedDashAreaState extends State<UnsubscribedDashArea> {
  List<DataRow> _rowList = [];
  bool loaded = false;
  Color unSubscribedColor = Colors.grey[300];

  List _daysInaWeek = ["M", "T", "W", "T", "F", "S", "S"];

  String percentageModifier(double value) {
    final roundedValue = value.ceil().toInt().toString();
    return '$roundedValue';
  }

  List<Widget> appBarActions = [
    Builder(
      builder: (context) => InkWell(
        child: Center(
          child: Container(
            height: 40.0,
            width: 40.0,
            margin: EdgeInsets.only(right: 8.0),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Image.asset(
                "assets/icons/userDefault.png",
                height: 25.0,
                width: 25.0,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
        onTap: () {
          print("Clicked");
          Scaffold.of(context).openEndDrawer();
        },
      ),
    ),
  ];

  Widget weeklyChart(BuildContext context) {
    final fromDate = DateTime(2021, 01, 10);
    final toDate = DateTime.now();

    final date1 = DateTime.now().subtract(Duration(days: 2));
    final date2 = DateTime.now().subtract(Duration(days: 3));

    return Container(
      // height: MediaQuery.of(context).size.height / 3,
      // width: MediaQuery.of(context).size.width,
      height: 250.0,
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: HexColor("#D4D4D4"),
        borderRadius: BorderRadius.circular(15),
      ),
      child: Stack(
        children: [
          BezierChart(
            fromDate: fromDate,
            bezierChartScale: BezierChartScale.WEEKLY,
            toDate: toDate,
            selectedDate: toDate,
            series: [
              BezierLine(
                dataPointStrokeColor: Colors.black,
                lineColor: Colors.white,
                label: "h",
                onMissingValue: (dateTime) {
                  if (dateTime.day.isEven) {
                    return 10.0;
                  }
                  return 5.0;
                },
                data: [
                  DataPoint<DateTime>(value: 10, xAxis: date1),
                  DataPoint<DateTime>(value: 50, xAxis: date2),
                ],
              ),
            ],
            config: BezierChartConfig(
              verticalIndicatorStrokeWidth: 3.0,
              verticalIndicatorColor: Colors.black26,
              showVerticalIndicator: true,
              verticalIndicatorFixedPosition: false,
              backgroundColor: HexColor("#D4D4D4"),
              footerHeight: 40.0,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 90.0,
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "DAILY",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                width: 90.0,
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "WEEK",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                width: 90.0,
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "MONTH",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  addDatatableRows() {
    setState(() {
      for (var i = 0; i < _daysInaWeek.length; i++) {
        _rowList.add(
          DataRow(
            cells: <DataCell>[
              DataCell(
                Text(
                  _daysInaWeek[i],
                  style: TextStyle(
                    color: unSubscribedColor,
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                  ),
                  // style: Theme.of(context).textTheme.subtitle2,
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: unSubscribedColor,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: unSubscribedColor,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              DataCell(
                Container(
                  width: 32.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
            ],
          ),
        );
      }
      loaded = true;
    });
  }

  @override
  void initState() {
    super.initState();
    addDatatableRows();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: HexColor("#FFF6F3"),
      appBar: MyAppBar(
        actions: appBarActions,
      ),
      endDrawer: Drawer(
        child: MyNavDrawer(),
      ),
      body: Container(
        child: ListView(
          children: [
            TopDesignWave("Dash"),
            Container(
              margin: EdgeInsets.only(bottom: 28.0),
              child: Column(
                children: [
                  Container(
                    // height: MediaQuery.of(context).size.height * 0.08,
                    width: MediaQuery.of(context).size.width * 0.95,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          HexColor("#831CD8"),
                          HexColor("#CF52D2"),
                        ],
                      ),
                    ),
                    child: Center(
                      child: MyTimer(),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Card(
                    elevation: 6.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height * 0.06,
                      width: MediaQuery.of(context).size.width * 0.65,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      padding: EdgeInsets.only(left: 10.0),
                      child: Row(
                        children: [
                          Container(
                            height: 15.0,
                            width: 15.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: Colors.purple,
                                width: 4.0,
                              ),
                            ),
                            margin: EdgeInsets.only(right: 30.0),
                          ),
                          Text(
                            "INI CET 2020",
                            style: TextStyle(
                              color: Colors.purple,
                              fontSize: 20.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Card(
                    elevation: 6.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height * 0.50,
                      width: MediaQuery.of(context).size.width * 0.95,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      height: 15.0,
                                      width: 15.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: unSubscribedColor,
                                          width: 4.0,
                                        ),
                                      ),
                                      margin: EdgeInsets.only(right: 5.0),
                                    ),
                                    Text(
                                      "Daily goals",
                                      style: TextStyle(
                                        fontFamily:
                                            "assets/fonts/Stolzl-Bold.ttf",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0,
                                        height: 1.4,
                                        color: unSubscribedColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 70.0,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  elevation: 2.0,
                                  child:
                                      Center(child: MySubscriptionButton("")),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  // color: Colors.amber,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: unSubscribedColor,
                                    width: 3.0,
                                  ),
                                ),
                                height: 150.0,
                                width: 150.0,
                              ),
                              Positioned(
                                top: 10.0,
                                left: 20.0,
                                child: Container(
                                  width: 110.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(1.0),
                                    // color: Colors.red.withOpacity(1.0),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 25.0,
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            Text(
                                              "0",
                                              style: TextStyle(
                                                color: unSubscribedColor,
                                                fontSize: 50.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Icon(
                                        Icons.timer,
                                        color: Colors.grey,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              for (var i = 0; i < _daysInaWeek.length; i++)
                                Container(
                                  height: 28.0,
                                  width: 28.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: unSubscribedColor,
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Card(
                    elevation: 6.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height * 0.45,
                      width: MediaQuery.of(context).size.width * 0.95,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      padding: EdgeInsets.all(18.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      height: 15.0,
                                      width: 15.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: unSubscribedColor,
                                          width: 4.0,
                                        ),
                                      ),
                                      margin: EdgeInsets.only(right: 5.0),
                                    ),
                                    Text(
                                      "Reading Stats",
                                      style: TextStyle(
                                        fontFamily:
                                            "assets/fonts/Stolzl-Bold.ttf",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0,
                                        height: 1.4,
                                        color: unSubscribedColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 70.0,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  elevation: 2.0,
                                  child:
                                      Center(child: MySubscriptionButton("")),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          weeklyChart(context),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Card(
                    elevation: 6.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.95,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      padding: EdgeInsets.all(18.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      height: 15.0,
                                      width: 15.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: unSubscribedColor,
                                          width: 4.0,
                                        ),
                                      ),
                                      margin: EdgeInsets.only(right: 5.0),
                                    ),
                                    Text(
                                      "Timetable",
                                      style: TextStyle(
                                        fontFamily:
                                            "assets/fonts/Stolzl-Bold.ttf",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0,
                                        height: 1.4,
                                        color: unSubscribedColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 70.0,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  elevation: 2.0,
                                  child:
                                      Center(child: MySubscriptionButton("")),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.white),
                            child: DataTable(
                              columnSpacing: 5.0,
                              horizontalMargin: 0.0,
                              dataRowHeight: 35.0,
                              columns: [
                                DataColumn(
                                  label: Text(
                                    'D/T',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '6am',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '9am',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '12pm',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '3pm',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '6pm',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '9pm',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '12am',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    '3am',
                                    style: TextStyle(color: unSubscribedColor),
                                  ),
                                ),
                              ],
                              rows: loaded ? _rowList : null,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: MyBottomNavBar(),
      floatingActionButton: MyFloatingButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
