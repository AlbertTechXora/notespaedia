import 'package:flutter/material.dart';

import '../ui/views/pdf_library/pdf_library.dart';

class BookCategories extends StatefulWidget {
  final List categoriesOnTop;
  final List categoriesOnBottom;
  BookCategories({this.categoriesOnTop, this.categoriesOnBottom});
  @override
  _BookCategoriesState createState() => _BookCategoriesState();
}

class _BookCategoriesState extends State<BookCategories> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // var maxWidth = constraints.maxWidth;
        return Container(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height * 0.08,
                height: 50.0,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.categoriesOnTop.length,
                  itemBuilder: (BuildContext context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => PDFLibrary(),
                          ),
                        );
                      },
                      child: Card(
                        elevation: 2.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                          decoration: BoxDecoration(
                            // color: Colors.white,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Center(
                            child: Row(
                              children: [
                                Image.asset(
                                  widget.categoriesOnTop[index]['image'],
                                  height: 35.0,
                                  width: 35.0,
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(
                                  widget.categoriesOnTop[index]['title'],
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height * 0.08,
                height: 50.0,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.categoriesOnBottom.length,
                  itemBuilder: (BuildContext context, index) {
                    return Card(
                      elevation: 2.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                        decoration: BoxDecoration(
                          // color: Colors.white,
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Center(
                          child: Row(
                            children: [
                              Image.asset(
                                widget.categoriesOnBottom[index]['image'],
                                height: 35.0,
                                width: 35.0,
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text(
                                widget.categoriesOnBottom[index]['title'],
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
