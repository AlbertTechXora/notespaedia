import 'package:flutter/material.dart';

import './mySubscriptionButton.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget leading;
  final Widget title;
  final List<Widget> actions;
  MyAppBar({this.leading, this.title, this.actions});

  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(50);
}

class _MyAppBarState extends State<MyAppBar> {
  List<Widget> appBarActions = [
    Builder(builder: (context) {
      return Row(
        children: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.shopping_cart_outlined),
          ),
          MySubscriptionButton("AppBar"),
        ],
      );
    }),
  ];

  @override
  Widget build(BuildContext context) {
    return AppBar(
      bottomOpacity: 0.0,
      automaticallyImplyLeading: false,
      key: widget.key,
      leading: widget.leading,
      title: widget.title,
      actions: widget.actions != null ? widget.actions : appBarActions,
    );
  }
}
