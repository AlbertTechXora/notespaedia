import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../views/libraryPage.dart';
import '../views/practicePage.dart';
import '../views/booksShelfPage.dart';
import '../views/dashAreaPage.dart';
import '../views/unsubscribedDashArea.dart';

class MyBottomNavBar extends StatefulWidget {
  @override
  _MyBottomNavBarState createState() => _MyBottomNavBarState();
}

class _MyBottomNavBarState extends State<MyBottomNavBar> {
  int selectedIndex;
  bool isDarkMode = false;
  // bool unSubscribed = true;
  bool unSubscribed = false;

  buttonActionsFn(selectedItemIndex) async {
    final preferences = await StreamingSharedPreferences.instance;
    preferences.setInt('BottomAppbarItemIndex', selectedItemIndex);
    print("selectedItemIndex:$selectedItemIndex");
    if (selectedItemIndex == 0) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => LibraryPage(),
        ),
      );
    } else if (selectedItemIndex == 1) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => BooksShelfPage(),
        ),
      );
    } else if (selectedItemIndex == 2) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => PracticePage(),
        ),
      );
    } else {
      unSubscribed
          ? Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => UnsubscribedDashArea(),
              ),
            )
          : Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => DashAreaPage(),
              ),
            );
    }
  }

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<int> bottomAppbarItemIndex =
        preferences.getInt('BottomAppbarItemIndex', defaultValue: 0);
    bottomAppbarItemIndex.listen((value) {
      selectedIndex = value;
    });
    var itemIndex = bottomAppbarItemIndex.getValue();
    setState(() {
      selectedIndex = itemIndex;
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 5.0,
      clipBehavior: Clip.antiAlias,
      child: Container(
        height: 56,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Image.asset(
                "assets/icons/home.png",
                height: 22.0,
                width: 22.0,
                color: selectedIndex == 0
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              onPressed: () {
                setState(() {
                  selectedIndex = 0;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            IconButton(
              icon: Image.asset(
                "assets/icons/books.png",
                height: 22.0,
                width: 22.0,
                color: selectedIndex == 1
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              onPressed: () {
                setState(() {
                  selectedIndex = 1;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            SizedBox(width: 40), // The dummy child
            IconButton(
              icon: Image.asset(
                "assets/icons/bullsEye.png",
                height: 22.0,
                width: 22.0,
                color: selectedIndex == 2
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              onPressed: () {
                setState(() {
                  selectedIndex = 2;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
            IconButton(
              icon: Image.asset(
                "assets/icons/person.png",
                height: 22.0,
                width: 22.0,
                color: selectedIndex == 3
                    ? Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .unselectedItemColor,
              ),
              onPressed: () {
                setState(() {
                  selectedIndex = 3;
                });
                buttonActionsFn(selectedIndex);
              },
            ),
          ],
        ),
      ),
    );
  }
}
