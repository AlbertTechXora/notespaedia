import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

// import './verifyNumDialogBox.dart';
// import './linkMobNumDialogBox.dart';
import './setDailyGoalDialogBox.dart';

class MyFloatingButton extends StatefulWidget {
  @override
  _MyFloatingButtonState createState() => _MyFloatingButtonState();
}

class _MyFloatingButtonState extends State<MyFloatingButton> {
  var sharedDailyGoalHrs = 0.0;
  var now = new DateTime.now();
  String percentageModifier(double value) {
    final roundedValue = value.ceil().toInt().toString();
    // return '$roundedValue';
    print("roundedValue: $roundedValue");
    return '';
  }

  checkWeekDay() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<int> weekDay = preferences.getInt('weekDay', defaultValue: 0);
    var sharedWeekDay = weekDay.getValue();
    // print("sharedWeekDay:$sharedWeekDay");
    if (sharedWeekDay != now.weekday) {
      preferences.setInt('weekDay', now.weekday);
      preferences.setDouble('dailyGoalHrs', 0);
      preferences.setInt('numBooksReadToday', 0);
    }
    getDailyGoalHrs();
  }

  getDailyGoalHrs() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<double> dailyGoalHrs =
        preferences.getDouble('dailyGoalHrs', defaultValue: 0.0);
    dailyGoalHrs.listen((value) {
      setState(() {
        sharedDailyGoalHrs = value;
      });
    });
    // print("sharedGetDailyGoalHrs: $sharedDailyGoalHrs");
  }

  @override
  void initState() {
    super.initState();
    checkWeekDay();
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Colors.white,
      onPressed: () {
        showDialog(
          context: context,
          // builder: (BuildContext context) => VerifyNumDialogBox(),
          // builder: (BuildContext context) => LinkMobNumDialogBox(),
          builder: (BuildContext context) => SetDailyGoalDialogBox(
            goalHrAlreadySet: sharedDailyGoalHrs,
          ),
        );
      },
      child: Stack(
        children: [
          SleekCircularSlider(
            initialValue: sharedDailyGoalHrs,
            max: 24.0,
            appearance: CircularSliderAppearance(
              angleRange: 360,
              startAngle: 270,
              infoProperties: InfoProperties(
                modifier: percentageModifier,
                mainLabelStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 10.0,
                  color: HexColor("#002231"),
                ),
              ),
              customColors: CustomSliderColors(
                trackColor: HexColor("#EBF5F5"),
                progressBarColors: [
                  HexColor("#30CCC8"),
                  HexColor("#70E9E4"),
                ],
              ),
              customWidths: CustomSliderWidths(progressBarWidth: 6.0),
            ),
            onChange: (double value) {
              // print(value);
            },
          ),
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            height: 60.0,
            width: 60.0,
          ),
          Positioned(
            top: 12.0,
            left: 10.0,
            child: Container(
              decoration: BoxDecoration(
                // color: Colors.amber,
                shape: BoxShape.circle,
              ),
              child: Image.asset(
                "assets/icons/lines.png",
                height: 35.0,
                width: 35.0,
              ),
            ),
          ),
        ],
      ),
      elevation: 2.0,
    );
  }
}
