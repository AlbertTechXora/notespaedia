import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import '../views/settingsPage.dart';
import '../views/newUpdatesPage.dart';

class MyNavDrawer extends StatefulWidget {
  @override
  _MyNavDrawerState createState() => _MyNavDrawerState();
}

class _MyNavDrawerState extends State<MyNavDrawer> {
  bool isDarkMode = false;

  getSharedDataPref() async {
    final preferences = await StreamingSharedPreferences.instance;
    Preference<bool> isDark =
        preferences.getBool('isDark', defaultValue: false);
    isDark.listen((value) {
      setState(() {
        isDarkMode = value;
      });
    });
    print("isDarkMode: $isDarkMode");
  }

  @override
  void initState() {
    super.initState();
    getSharedDataPref();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // var maxWidth = constraints.maxWidth;
        return Container(
          color: Theme.of(context).primaryColor,
          child: ListView(
            children: <Widget>[
              Container(
                color: isDarkMode ? Colors.black : Colors.white,
                child: Stack(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: [
                          Container(
                            color: HexColor("#4DC9C7"),
                            height: 210.0,
                          ),
                          Container(
                            child: Image.asset(
                              "assets/images/topDesign.png",
                              // height: 360.0,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 30.0,
                      left: 40.0,
                      right: 40.0,
                      child: Container(
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50.0),
                                  color: Colors.white,
                                ),
                                height: 100.0,
                                width: 100.0,
                                child: ClipOval(
                                  child: Align(
                                    heightFactor: 1,
                                    widthFactor: 1,
                                    child: Image.asset(
                                      "assets/icons/userDefault.png",
                                      height: 70.0,
                                      width: 70.0,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 12.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Profile Name',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              'Academic Year',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => SettingsPage(),
                                  ),
                                );
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    "assets/icons/cog.png",
                                    height: 22.0,
                                    width: 22.0,
                                    fit: BoxFit.contain,
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Text(
                                    "My Profile",
                                    style: TextStyle(
                                      color: HexColor("#25605E"),
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                color: isDarkMode ? Colors.black : Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/notification.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "Notifications",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/Coins.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "Pro Plans",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/clock.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "Coming Soon",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/sync.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "New Updates",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => NewUpdatesPage(),
                          ),
                        );
                      },
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/ourAffiliates.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "Our Affiliates",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/referAndExtend.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "Refer & Extend",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/messages.png",
                        height: 20.0,
                        width: 20.0,
                        fit: BoxFit.contain,
                      ),
                      title: Text(
                        "FAQ",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      onTap: () {},
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 5.0, 0, 5.0),
                      child: Text(
                        "Share the app",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          // fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 0, 0, 5.0),
                      child: Text(
                        "Rate Us",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          // fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 0, 0, 5.0),
                      child: Text(
                        "About Us",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          // fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 0, 0, 5.0),
                      child: Text(
                        "Contact Us",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          // fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 0, 0, 5.0),
                      child: Text(
                        "T&C",
                        style: TextStyle(
                          color: isDarkMode
                              ? Theme.of(context).primaryColorLight
                              : Theme.of(context).primaryColor,
                          // fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Version 1.0.1",
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
