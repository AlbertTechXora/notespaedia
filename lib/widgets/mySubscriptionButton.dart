import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

import '../views/subscriptionPage.dart';
import './gradientText.dart';

class MySubscriptionButton extends StatefulWidget {
  final String callFrom;
  MySubscriptionButton(this.callFrom);
  @override
  _MySubscriptionButtonState createState() => _MySubscriptionButtonState();
}

class _MySubscriptionButtonState extends State<MySubscriptionButton> {
  Color unSubscribedColor = Colors.grey[300];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => SubscriptionPage(),
          ),
        );
      },
      child: Container(
        height: 38.0,
        width: 70.0,
        margin: widget.callFrom == "AppBar"
            ? EdgeInsets.only(right: 8.0)
            : EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(18.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.callFrom != "AppBar"
                ? Icon(
                    Icons.lock,
                    size: 15.0,
                    color: unSubscribedColor,
                  )
                : Container(),
            GradientText(
              'PRO',
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              gradient: LinearGradient(colors: [
                HexColor("#FF3EBC"),
                HexColor("#FF50AF"),
                HexColor("#FF69A0"),
                HexColor("#FF878E"),
                HexColor("#FE9F85"),
                HexColor("#FFC070"),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
