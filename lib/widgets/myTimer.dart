import 'dart:async';

import 'package:flutter/material.dart';

class MyTimer extends StatefulWidget {
  @override
  _MyTimerState createState() => _MyTimerState();
}

class _MyTimerState extends State<MyTimer> {
  DateTime _now;
  DateTime _auction;
  Timer _timer;

  @override
  void dispose() {
    // Cancels the timer when the page is disposed.
    _timer.cancel();

    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    // Sets the current date time.
    _now = DateTime.now();
    // Sets the date time of the auction.
    _auction = _now.add(Duration(days: 1));

    // Creates a timer that fires every second.
    _timer = Timer.periodic(
      Duration(
        seconds: 1,
      ),
      (timer) {
        setState(() {
          // Updates the current date time.
          _now = DateTime.now();

          // If the auction has now taken place, then cancels the timer.
          if (_auction.isBefore(_now)) {
            timer.cancel();
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Calculates the difference between the auction date time and the current date time.
    final difference = _auction.difference(_now);
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 5.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                '${difference.inDays}',
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.amber,
                ),
              ),
              Text(
                'Days ',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                '${difference.inHours}',
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.amber,
                ),
              ),
              Text(
                'Hours ',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                '${difference.inMinutes.remainder(60)}',
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.amber,
                ),
              ),
              Text(
                'Minutes ',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                '${difference.inSeconds.remainder(60)}',
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.amber,
                ),
              ),
              Text(
                'Sec',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                ),
              ),
            ],
          ),
          SizedBox(
            width: 5.0,
          ),
        ],
      ),
    );
  }
}
