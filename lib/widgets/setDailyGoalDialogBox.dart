import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sqflite/sqflite.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:path/path.dart' as Path;

import '../models/connections.dart' as Constants;

class TimingHours {
  static List<Icon> problemListIcons = [];
  static List<String> timingHoursList = [];
}

class SetDailyGoalDialogBox extends StatefulWidget {
  final double goalHrAlreadySet;
  SetDailyGoalDialogBox({
    @required this.goalHrAlreadySet,
  });
  @override
  _SetDailyGoalDialogBoxState createState() => _SetDailyGoalDialogBoxState();
}

class _SetDailyGoalDialogBoxState extends State<SetDailyGoalDialogBox> {
  var titleText = "Set your Daily goal";
  var subtitleText = "Set daily reading time to\ntrack your performance";
  var buttonText = "Done";

  int _selectedHrsIndex = 0;
  double itemExtent = 40.0;
  FixedExtentScrollController scrollController;

  var dailyGoalHrs;
  var now = new DateTime.now();

  setDailyGoal() async {
    final preferences = await StreamingSharedPreferences.instance;
    var dailyGoalHrsString = dailyGoalHrs.split(' ');
    double goalHrs = double.parse(dailyGoalHrsString[0]);
    print("dailyGoalHrs: $goalHrs");
    preferences.setDouble('dailyGoalHrs', goalHrs);
    _insertDailyGoalHrs(goalHrs);
  }

  _insertDailyGoalHrs(goalHrs) async {
    print("Starting _insertDailyGoalHrs...");
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = Path.join(databasesPath, Constants.dbName);
    String dailyGoalsTable = Constants.dailyGoalsTbl;

    // open the database
    Database database = await openDatabase(
      path,
      version: 1,
    );
    // Deleting records to avoid duplicate entries same day.
    await database
        .execute('DELETE FROM $dailyGoalsTable WHERE WeekDay=${now.weekday}');

// Insert some records in a transaction
    await database.transaction((txn) async {
      int dailyGoalsTableId = await txn.rawInsert(
          'INSERT INTO $dailyGoalsTable(DailyGoalsHrs, WeekDay, Month, Year) VALUES($goalHrs,${now.weekday},${now.month},${now.year})');
      print('dailyGoalsTableId: $dailyGoalsTableId');
    });
  }

  @override
  void initState() {
    TimingHours.timingHoursList.clear();
    for (var j = 0; j <= 48; j++) {
      var t = (j / 2).toString();
      t += " hrs";
      TimingHours.timingHoursList.add(t);
    }
    // print(TimingHours.timingHoursList);
    print("goalHrAlreadySet: ${widget.goalHrAlreadySet}");
    if (TimingHours.timingHoursList
        .contains("${widget.goalHrAlreadySet}" + " hrs")) {
      var ind = TimingHours.timingHoursList
          .indexOf("${widget.goalHrAlreadySet}" + " hrs");
      print("ind:$ind");
      scrollController = FixedExtentScrollController(initialItem: ind);
    } else {
      print("ind:0");
      scrollController =
          FixedExtentScrollController(initialItem: _selectedHrsIndex);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(
        left: 15.0,
        right: 15.0,
      ),
      backgroundColor: Colors.white,
      insetAnimationDuration: const Duration(milliseconds: 100),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Container(
        height: 330.0,
        child: ListView(
          children: [
            SizedBox(
              height: 20.0,
            ),
            Container(
              height: 35.0,
              width: 35.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.green,
                  width: 8.0,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              titleText,
              style: Theme.of(context).textTheme.headline1,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              subtitleText,
              style: Theme.of(context).textTheme.bodyText2,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              height: 100.0,
              width: 100.0,
              child: TimingHours.timingHoursList.isEmpty
                  ? Container()
                  : CupertinoPicker(
                      scrollController: scrollController,
                      itemExtent: itemExtent,
                      children: <Widget>[
                        for (var i = 0;
                            i < TimingHours.timingHoursList.length;
                            i++)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Text(
                                  TimingHours.timingHoursList[i],
                                  style: Theme.of(context).textTheme.headline1,
                                ),
                              )
                            ],
                          ),
                      ],
                      onSelectedItemChanged: (int index) {
                        print(TimingHours.timingHoursList[index]);
                        setState(() {
                          dailyGoalHrs = TimingHours.timingHoursList[index];
                        });
                      },
                      looping: true,
                    ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    setDailyGoal();
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    buttonText,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
