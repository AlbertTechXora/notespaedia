import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_color/flutter_color.dart';

class SocialLoginButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 80.0,
            child: ElevatedButton(
              onPressed: () {},
              child: FaIcon(
                FontAwesomeIcons.google,
                color: Colors.white,
                size: 18.0,
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                  HexColor("#D84537"),
                ),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 5.0,
          ),
          SizedBox(
            width: 80.0,
            child: ElevatedButton(
              onPressed: () {},
              child: FaIcon(
                FontAwesomeIcons.apple,
                color: Colors.white,
                size: 20.0,
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                  HexColor("#212121"),
                ),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 5.0,
          ),
          SizedBox(
            width: 80.0,
            child: ElevatedButton(
              onPressed: () {},
              child: FaIcon(
                FontAwesomeIcons.facebook,
                color: Colors.white,
                size: 20.0,
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                  HexColor("#1577F1"),
                ),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
