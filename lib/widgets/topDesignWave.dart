import 'package:flutter/material.dart';

class TopDesignWave extends StatelessWidget {
  final String pageTitle;
  TopDesignWave(this.pageTitle);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // var maxWidth = constraints.maxWidth;
        return Container(
          child: Stack(
            children: [
              Image.asset("assets/images/topDesign.png"),
              Positioned(
                left: 10.0,
                top: 35.0,
                child: Container(
                  child: Text(
                    pageTitle,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
